<form method="post" action="searchrestaurant">
     <div class="row">
      <div class="col-md-2"><a id="stickyLogo" href="../"><img src="resources/img/rezzit21-logo.png" class="img-responsive" ></a></div>
      <div class="col-md-8">
	<div class="row">
                <div class="col-md-2 styled-select">
                    <select name="people">
		        <option value="1" >1 person</option>
			<option value="2" selected>2 people</option>
			<option value="3" >3 people</option>
			<option value="4" >4 people</option>
			<option value="5" >5 people</option>
			<option value="6" >6 people</option>
			<option value="7" >7 people</option>
			<option value="8" >8 people</option>
			<option value="9" >9 people</option>
			<option value="10" >10 people</option>
			<option value="11" >11 people</option>
			<option value="12" >12 people</option>
			<option value="13" >13 people</option>
			<option value="14" >14 people</option>
			<option value="15" >15 people</option>
			<option value="16" >16 people</option>
			<option value="17" >17 people</option>
			<option value="18" >18 people</option>
			<option value="19" >19 people</option>
			<option value="20" >20 people</option>
                    </select>
                 </div>
                 <div class="col-md-3">
			<input class="the_date" type="text" name="date" value="<?=date("m/d/Y");?>" style="width:100%">
                 </div>
        <div class="col-md-2 styled-select">
                        <select name="hour">
                            <?PHP

                           $hour = date('g');
                            $hour = ($hour+1).":00".date('a');
                            echo $hour;
//                            echo $hour;
                            function iterateIntervals($given_hour, $i, $fifteen_intervals, $am_or_pm){
                                $selected = '';
                                foreach($fifteen_intervals as $interval){
                                  $hour = $i . ":" . $interval . $am_or_pm;
                                  if ($given_hour  == $hour)
                                  {
                                    $selected = 'selected';
                                  }
                                   echo '<option value="' . $hour .'" ' .$selected . '>' . $hour . '</option>';
                                   $selected = '';
                               }
                            }
                            $fifteen_intervals = array("00", "15", "30", "45");
                            iterateIntervals($hour, "12", $fifteen_intervals, "am");
                            for ($i = 1; $i <= 11; $i++)
                            {
                                iterateIntervals($hour, $i, $fifteen_intervals, "am");
                            }
                            iterateIntervals($hour, "12", $fifteen_intervals, "pm");
                            for ($i = 1; $i <= 11; $i++)
                            {
                                iterateIntervals($hour, $i, $fifteen_intervals, "pm");
                            }
                            ?>
                        </select>
	         </div>

		 <div class="col-md-4">
		    <input type="text" name="search" placeholder="Location or Restaurant">
		</div>
		<div class="col-md-1"> 
		    <button><span class="glyphicon glyphicon-search btn btn-default"></span></button>
		</div>
             </div>
         </div>
         <div class="col-md-3"></div>
    </div>
</form> 
