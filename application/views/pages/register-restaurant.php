<div class="container">
  <h2>Register Your Restaurant</h2>
  <form>
     <div class="row">
      <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <input data-validate="required" name="restaurant_name" placeholder="Restaurant Name"></input>     
            </div>
          </div>
          <div class="row">
             <div class="col-md-12">
               <input data-validate="required" name="contact_first_name" placeholder="Contact first name"></input>
             </div>
          </div>
          <div class="row">
             <div class="col-md-12">
               <input data-validate="required" name="contact_last_Name" placeholder="Contact last name"></input>
             </div>
          </div>
          <div class="row">
             <div class="col-md-12">
                <input data-validate="required,email" name="contact_email" placeholder="Contact email"></input>
             </div>
          </div>
      </div>
      <div class="col-md-6">
          <div class="row">
              <div class="col-md-12">
                   <input name="job_title" placeholder="Job title"></input>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                   <input name="address_one" placeholder="Address #1"></input>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                   <input name="address_two" placeholder="Address #2"></input>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                   <input data-validate="required" name="city" placeholder="City"></input>
              </div>
          </div>
          <div class="row">
              <div class="col-md-5">
                   <select data-validate="required" name="state"><?=$states?></select>
              </div>
              <div class="col-md-7">
                  <input data-validate="required,number,min(5)" name="zip" placeholder="Zip"></input>
              </div>
          </div>
          <div class="row">
              <div class="col-md-12">
                  <input data-validate="url" name="company_website" placeholder="Company website"></input>
              </div>
          </div>
      </div>
     </div>
     <div class="row">
         <input data-validate="required" type="checkbox">I agree to Rezzit<sup>21</sup> Terms and Conditions</input>
     </div>
     <div class="row">
         <button type="submit" >Submit</button>
     </div>
  </form>
</div>
