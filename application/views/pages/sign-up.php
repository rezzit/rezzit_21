<div class="container">
  <h2> Sign Up!</h2>
    <p><?php echo $_SESSION['error']; ?></p>
  <form action="signup/createSignup" method="post">
     <div class="row">
        <div class="col-md-12">
           <input data-validate="required" name="firstName" placeholder="First Name"></input>
        </div>
     </div>
     <div class="row">
        <div class="col-md-12">
           <input data-validate="required" name="lastName" placeholder="Last Name"></input>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
	   <input data-validate="required,email" name="email" placeholder="Email"></input>
        </div>
    </div>
     <div class="row">
        <div class="col-md-12">
	   <input type="password" data-validate="required,min(12)" name="password" placeholder="Password"></input>
        </div>
     </div>
     <div class="row">
       <div class="col-md-12">
	   <input type="password" data-validate="confirmPassword" name="confirm_password" placeholder="Confirm password"></input>
        </div>
     </div>
      <div class="row">
        <div class="col-md-12">
	   <input type="checkbox">I agree to Rezzit<sup>21</sup> Terms &amp; Conditions</input>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
	   <button>Sign up!</button>
        </div>
      </div>
     </div>
  </form>
</div> 
