<div class="container">

<div style="margin-top: 50px" class="row">
     <div class="span4">
	  <form>
            <select>People</select>
	    <select>Date</select>
	    <select>Time</select>
	    <input placeholder='Restaurant'></input>
	    <button>Search</button>
	  </form> 
     </div>
   </div>


  <!-- Headings & Paragraph Copy -->
  <div class="row">
    <div class="span4">
      <h3>Typographic scale</h3>
      <p>The entire typographic grid is based on two Less variables in our variables.less file: <code>@baseFontSize</code> and <code>@baseLineHeight</code>. The first is the base font-size used throughout and the second is the base line-height.</p>
      <p>We use those variables, and some math, to create the margins, paddings, and line-heights of all our type and more.</p>
    </div>
    <div class="span4">
      <h3>Example body text</h3>
      <p>Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
      <p>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec sed odio dui.</p>
    </div>
  </div>
 </div> 
