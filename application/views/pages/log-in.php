<div class="container">
  <h2>Login</h2>
  <form action="login/signin" method="post">
      <p>
          <?php echo $_SESSION['error'];?>
      </p>
     <div class="row">
      <div class="col-md-12">
      	<input data-validate="required,email" name="email" placeholder="Email"></input>
      </div>
     </div>
     <div class="row">
      <div class="col-md-12">
      	<input type="password" data-validate="required" name="password" placeholder="Password"></input>
      </div>
     </div>
     <div class="row">
      <div class="col-md-12">
      	<button>Login</button>
      </div>
     </div>
     <div class="row">
      <div class="col-md-12">
      	<a href="/renewpassword">Forgot your password?</a>
      </div>
     </div>
  </form>
</div>
