<style>
    #main{
        background: white;
    }
    .grid{
        text-align: center;
        width: 800px;
        margin: 5% auto;
        padding: 10% 5%;
        border: thin solid #ccc;
    }
</style>
<div class="container grid">
    <!-- Headings & Paragraph Copy -->
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            Thank you for your reservation.
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            Name: <?=$_SESSION["firstName"];?> <?=$_SESSION["lastName"];?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            Email: <?=$_SESSION["email"];?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            Time: <?=$_SESSION['time'];?>
        </div>
    </div>
 </div> 
