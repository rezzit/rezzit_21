<style>
    .loader{
        background-color: #5ec1d8;
        position: fixed;
        z-index: 100000000000000 !important;
        left: 0;
        top: 0;
        padding: 18% 0;
        width: 100%;
        text-align: center;
    }
    .loader div{
        width: 800px;
        margin: auto;
        text-align: center;
        display: block;
        color: #eee;
        font-weight: bold;
    }
    .loader img{
        margin: auto;
    }
    #main{
        padding: 0 !important;
        margin: 0 !important;
        background: white;
    }
    .ghostBtn{
        padding: 5px;
        border: thin solid white;
    }
    #headerContainer{
        background:url(resources/img/perrys-rotator-pe-20140415-1600x500.png);
        margin: 0;
        width: 100%;
        color: white;
        padding: 1% 1%;
        text-align: center;
        background-attachment: fixed;
        background-position: top left;
        background-size: cover;
    }
    .heroArea{
        padding: 10% 0 0 0;
    }
    .styled-select select {
        background: transparent;
        font-size: 16px;
        line-height: 1;
        border: 0;
        border-radius: 0;
        -webkit-appearance: none;
        color:black;
    }
    .styled-select {
        padding: 20px 10px;
        overflow: hidden;
        background: #fff url("http://www.scottgood.com/jsg/blog.nsf/images/arrowdown.gif") no-repeat 90% 50%;
        border: 1px solid #ccc;
    }
    input{
        padding:19px;
        color: #000;
    }
    .styleMe{
        background: url(resources/img/secondImg.png);
        background-size: cover;

        height: 250px;
        margin: 15px;
        margin-top: 0;
        padding: 15px 30px;

        width: Calc(23% - 15px);
        color: white;

    }
    .fluid-container{
        padding: 0 30px;
    }
    .mapContainer{
        background-size: cover;
        height: 800px;
        /* margin: 15px; */
        width: 49%;
        color: white;
        padding: 0;
        display: inline-block;
        float: none;
        vertical-align: top;
        border-left: 1px solid #ccc;
    }
    .stickyMap{
        background-size: cover;
        height: Calc(100% - 64px);
        /* margin: 15px; */
        width: 50%;
        color: white;
        padding: 0;
        display: inline-block;
        float: none;
        vertical-align: top;
        position: fixed;
        top: 64px;
        right: 0;
    }
    #map_canvas{
        height: 100%;
        width: 100%;
    }
    .styleMe div{

    }
    .styleMe .bCaption{
        position: absolute;
        bottom: 15px;
        left: 15px;
    }
    .styleMe .col-md-6{
        font-size: 12px;
        border: thin solid white;

        padding: 3px;
        text-align: center;
    }
    #regForm .col-md-3, #regForm .col-md-4, #regForm .col-md-1{
        padding: 0;
    }
    #regForm button {
        background: #71B2D8;
        color: white;
        padding: 15px 5px;
        border: none;
        height: 62px;
    }
    .floater{
        margin: 50px 0;
        background: none;
        color: white;
        padding: 10px 20px;
    }
    #regForm .btn-default{
        color: #ffffff;
        background: none;
        border: none;
    }
    #regForm button{
        width: 100%;
    }
    #regForm form{
        margin: 30px 0;
    }

    .description .btn-default{
        display: block;
        margin: 0px auto;
        margin-top: 20px;
    }
    .sticky{
        position: fixed;
        top:0;
        left:0;
        width: 100%;
        z-index: 100000;
        background: white;
        border-bottom: 1px solid #ccc;
    }
    .sticky input{
        padding: 20px;
    }
    .sticky button {
        height: 62px;
    }
    .sticky .styled-select, .sticky input{
        border:none;
        border-left: thin solid #ccc;
    }
    .sticky form{
        margin: 0 !important;
    }
    .navi span{
        margin: 12px;
        font-size: 16px;
    }
    .navi span a{
        color:white;
    }
    .mapWidth{
        width: 50% !important;
        display: inline-block !important;
    }
    .mapCols{
        width: Calc(50% - 30px);
    }
    .mapWidth .stickyAction{
        width: 50% !important;
    }
    .stickyAction{
        position: fixed;
        top:64px;
        left: 0;
        width: 100%;
        margin: 0 !important;
        z-index: 10000 !important;
    }
    .actionArea{
        border-bottom:thin solid #ccc;
        margin: auto -33px;
        background: rgb(238, 238, 238);
        margin-bottom: 30px;
    }
    .actionArea .col-md-2{
        border-left: thin solid #ccc;
        padding: 20px;
        text-align: center;
    }
    .overlay{
        opacity:0.1;
    }
    .picker{
        border: thin solid #ccc;
        background: rgb(238, 238, 238);
        z-index: 9999 !important;
        padding: 0;
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        color: rgb(51, 51, 51);
    }
    .picker p{
        background:#71B2D8;
        padding: 10px;
        border-bottom: thin solid #ccc;
        border-top: thin solid #ccc;
        text-align: center;
        margin-bottom: 20px;
    }
    .picker p:first-child {
        position: relative;
        left:10px;
    }
    .picker div{
        margin-bottom: 20px;
        text-align: center;
    }
    #stickyLogo img{
        text-align: center;
        margin: auto;
        padding-top: 5%;
    }
    .closeMe{
        position: absolute;
        right: 10px;
        top: 5px;

        width: 20px;
        text-align: center;
        font-weight: bold;
        font-size: 20px;
    }
    @media (max-width: 800px) {
        .col-xs-12 {
            width: 100% !important;
            height: 250px !important;
        }
    }
</style>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false">
</script>
<script type="text/javascript">
//    var locations = [
//        ['Location 1 Name', 'New York, NY', 'Location 1 URL'],
//        ['Location 2 Name', 'Newark, NJ', 'Location 2 URL'],
//        ['Location 3 Name', 'Philadelphia, PA', 'Location 3 URL']
//    ];
//
var locations;
    var geocoder;
    var map;
    var bounds = new google.maps.LatLngBounds();
//
//    function initialize() {
//        map = new google.maps.Map(
//            document.getElementById("map_canvas"), {
//                center: new google.maps.LatLng(37.4419, -122.1419),
//                zoom: 13,
//                mapTypeId: google.maps.MapTypeId.ROADMAP
//            });
//        geocoder = new google.maps.Geocoder();
//
//        for (i = 0; i < locations.length; i++) {
//
//
//            geocodeAddress(locations, i);
//        }
//    }
//    google.maps.event.addDomListener(window, "load", initialize);

    function geocodeAddress(locations, i) {
        var title = locations[i][0];
        var address = locations[i][1];
        var url = locations[i][2];
        geocoder.geocode({
                'address': locations[i][1]
            },

            function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    var marker = new google.maps.Marker({
                        icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                        map: map,
                        position: results[0].geometry.location,
                        title: title,
                        animation: google.maps.Animation.DROP,
                        address: address,
                        url: url
                    })
                    infoWindow(marker, map, title, address, url);
                    bounds.extend(marker.getPosition());
                    map.fitBounds(bounds);
                } else {
                    alert("geocode of " + address + " failed:" + status);
                }
            });
    }

    function infoWindow(marker, map, title, address, url) {
        google.maps.event.addListener(marker, 'click', function () {
            var html = "<div><h3>" + title + "</h3><p>" + address + "<br></div><a href='" + url + "'>View location</a></p></div>";
            iw = new google.maps.InfoWindow({
                content: html,
                maxWidth: 350
            });
            iw.open(map, marker);
        });
    }

    function createMarker(results) {
        var marker = new google.maps.Marker({
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
            map: map,
            position: results[0].geometry.location,
            title: title,
            animation: google.maps.Animation.DROP,
            address: address,
            url: url
        })
        bounds.extend(marker.getPosition());
        map.fitBounds(bounds);
        infoWindow(marker, map, title, address, url);
        return marker;
    }

</script>
<script>
    alert = function() {};


    $(document).ready(function(){
        $(".styleMe").each(function(){
            var ref = $(this);
            $.ajax({
                type: "POST",
                url: "searchrestaurant/getAvailSeats?id="+$(this).attr('data-restId')+"&time=<?=$_SESSION['time']?>&endTime=<?=$_SESSION['endTime'];?>",
                data: {
                },

                success: function(data, textStatus, jqXHR)
                {
                    var array = JSON.parse(data);
                    console.log(data);
                    array.forEach(function(object) {

                        $(ref).find(".picker").append('<p>'+object.name+'</p>');

                        $(".closeMe").click(function(){
                           $(this).parents(".picker").hide();
                        });
                        $(".checkAvail").click(function(){
                           $(this).parents(".styleMe").find(".picker").show();
                        });
                        if($(ref).attr('data-max') < object.avail_seats){
                            $(ref).attr('data-max', object.avail_seats);
                        }

                        if(object.avail_seats > 0){
                            $(ref).find(".picker").append('<div><a href="restaurantdetails?id='+$(ref).attr('data-restId')+'">'+'<button class="btn btn-default">Book now</button>'+'</a></div>');
                        }else{
                            $(ref).find(".picker").append('<div>No bookings available</div>');
                        }
                    });
                    if($(ref).attr('data-max') == 0){
                        $(ref).addClass('overlay').parents("a").attr("href","");
                    }
                    $(".loader").fadeOut(500);
                }
            });
        });

        $('.styleMe').hover(

            function () {
                $(this).find(".picker").show();
            },

            function () {
                $(this).find(".picker").hide();
            }
        );

        $(".picker").hide();
        $('nav').hide();
        $('footer').hide();
        $('.mapContainer').toggle();

        var query = '<?=$_POST['search']?>';
        query = query.toLowerCase();
        if(query){
            $('.styleMe').hide();
            $('.styleMe[data-name*="'+query+'"]').show();
            $('.styleMe[data-city*="'+query+'"]').show();
            $('.styleMe[data-state*="'+query+'"]').show();
            $('.styleMe[data-address*="'+query+'"]').show();
            $('.styleMe[data-zip*="'+query+'"]').show();


        }
        $('form[action="searchrestaurant"]').appendTo('#regForm');
        var stickyNavTop = $('#regForm').offset().top;

        var stickyNav = function(){
            var scrollTop = $(window).scrollTop();

            if (scrollTop > stickyNavTop) {
                $('#regForm').addClass('sticky');
                $('#stickyLogo').show();
            } else {
                $('#regForm').removeClass('sticky');
                $('#stickyLogo').hide();
            }
        };

        stickyNav();

        var stickyMapTop = $('.fluid-container').offset().top;

        var stickyMap = function(){
            var scrollTop = $(window).scrollTop();

            if ((scrollTop) > stickyNavTop) {
                $('.mapContainer').addClass('stickyMap');
            } else {
                $('.mapContainer').removeClass('stickyMap');
            }
        };

        stickyMap();

        var stickyMapTop = $('.fluid-container').offset().top;

        var stickyAction = function(){
            var scrollTop = $(window).scrollTop();

            if ((scrollTop) > stickyNavTop) {
                $('.actionArea').addClass('stickyAction');
                $('#containerArea').css("marginTop","200px");
            } else {
                $('.actionArea').removeClass('stickyAction');
                $('#containerArea').css("marginTop","0");
            }

        };

        stickyAction();

        $(window).scroll(function() {
            stickyNav();
            stickyMap();
            stickyAction();
        });
        $('.mapIt').click(function(){
           $("#containerArea .col-md-3").toggleClass("mapCols");
           $("#containerArea").toggleClass("mapWidth");
            $('.mapContainer').toggle();

            if($('.mapContainer:visible')){
                locations = [
                ];

                $('.styleMe:visible').each(function(){
                    locations.push([
                        $(this).attr('data-name'),
                        $(this).attr('data-address')+", "+$(this).attr('data-city')+", "+$(this).attr('data-state')+", "+$(this).attr('data-zip'),
                        $(this).parents('a').attr('href')
                    ]);
                });

                map = new google.maps.Map(
                    document.getElementById("map_canvas"), {
                        center: new google.maps.LatLng(37.4419, -122.1419),
                        zoom: 13,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
                geocoder = new google.maps.Geocoder();

                for (i = 0; i < locations.length; i++) {


                    geocodeAddress(locations, i);
                }
            }


        });
        $("#containerArea .col-md-3").toggleClass("mapCols");
        $("#containerArea").toggleClass("mapWidth");
        $('.mapContainer').toggle();

        if($('.mapContainer:visible')){
            locations = [
            ];

            $('.styleMe:visible').each(function(){
                locations.push([
                    $(this).attr('data-name'),
                    $(this).attr('data-address')+", "+$(this).attr('data-city')+", "+$(this).attr('data-state')+", "+$(this).attr('data-zip'),
                    $(this).parents('a').attr('href')
                ]);
            });

            map = new google.maps.Map(
                document.getElementById("map_canvas"), {
                    center: new google.maps.LatLng(37.4419, -122.1419),
                    zoom: 13,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });
            geocoder = new google.maps.Geocoder();

            for (i = 0; i < locations.length; i++) {


                geocodeAddress(locations, i);
            }
        }
    });
</script>
<div class="loader">
    <div>
        <img src="http://rezzit21.com/resources/img/Rezzite%20Logo%20Large%20White.png" class="img-responsive animated infinite pulse">
        <p>
            Almost there! Finding available reservations.
        </p>
    </div>
</div>
<div id="headerContainer" class="container">
    <div class="row">
        <div class="col-md-2">
            <img src="resources/img/rez21White.png" class="img-responsive" />
        </div>
        <div class="navi col-md-5 col-md-offset-5">
      <span>
        <a href="Features">
            Features
        </a>
      </span>
      <span>
        <a href="Features">
            Diners
        </a>
      </span>
            <span>
        <a href="Features">
            Restaurant Owners
        </a>
      </span>
            <span>
        <a href="#" class="loginBtn">
            Login
        </a>
      </span>
            <span>
        <a href="#" class="signupBtn">
            Sign Up
        </a>
      </span>
        </div>
    </div>

    <div class="row heroArea">
        <div class="col-md-12">

            <h1>Define Your Dining Experience</h1>
            <button class="btn-default btn ghost floater">Reserve Your Seat</button>
            <div id="regForm"></div>
        </div>
    </div>
</div>

<div class="fluid-container" id="containerArea">
    <div class="row actionArea">
        <div class="col-md-2  col-md-offset-6">
            <div class="mapIt">Sort <span class="glyphicon glyphicon-sort"></span></div>
        </div>

        <div class="col-md-2">
            <div class="mapIt">Filter <span class="glyphicon glyphicon-filter"></span></div>
        </div>

        <div class="col-md-2">
            <div class="mapIt">Map <span class="glyphicon glyphicon-map-marker"></span></div>
        </div>
    </div>
<div class="row">

    <?php
        foreach($revProfile as $restaurant) {
//            echo '<a href="restaurantdetails?id='.$restaurant->getObjectId().'">';
            echo '<div class="col-md-3 styleMe col-xs-12"
            data-name="'.strtolower($restaurant->name).'"
            data-city="'.strtolower($restaurant->city).'"
            data-state="'.strtolower($restaurant->state).'"
            data-address="'.strtolower($restaurant->address_1).'"
            data-zip="'.strtolower($restaurant->zip).'"
            data-restId="'.$restaurant->getObjectId().'"
            data-max="0"
            >';
                echo '<div class="picker"><span class="closeMe">X</span></div>';
                // Header

                echo '<div class="row">';
                    echo '<div class="col-md-6 checkAvail">';
                        echo 'Check Availability';
                    echo '</div>';
                    echo '<div class="col-md-1 col-md-offset-3">';
                        echo '<span class="glyphicon glyphicon-heart ghostBtn"></span>';
                    echo '</div>';
                echo '</div>';

                // Header

                // Body



                echo '<div class="row bCaption">';
                    echo '<div class="col-md-12">';
                        echo '<h4>';
                            echo $restaurant->name;
                        echo '</h4>';
                        echo '<div>';
                            echo $restaurant->name; // Reviews
                                echo ' | ';
                            echo '$$$';
                        echo '</div>';
                        echo '<small>';
                            echo $restaurant->address_1;
                            echo ' - ';
                            echo $restaurant->city;
                            echo ', ';
                            echo $restaurant->state;
                            echo ', ';
                            echo $restaurant->zip;
                        echo '</small>';
                    echo '</div>';
                echo '</div>';



                // Body
            echo '</div>';
//            echo '</a>';
        }
    ?>
</div>
<!--  <table id="restaurant-data-table" class="display" cellspacing="0" width="100%">-->
<!--        <thead>-->
<!--            <tr>-->
<!--                <th>Name</th>-->
<!--                <th>Location</th>-->
<!--                <th>Phone</th>-->
<!--            </tr>-->
<!--        </thead>-->
<!--        <tfoot>-->
<!--            <tr>-->
<!--                <th>Name</th>-->
<!--                <th>Location</th>-->
<!--                <th>Phone</th>-->
<!--            </tr>-->
<!--        </tfoot>-->
<!--        <tbody>-->
        <?php

//        foreach($revProfile as $restaurant){
//            echo '<tr>';
//
//            echo '<td>';
//                echo '<a href="restaurantdetails?id='.$restaurant->getObjectId().'">';
//                    echo $restaurant->name;
//                echo '</a>';
//            echo '</td>';
//
//            echo '<td>';
//            echo $restaurant->address_1." - ".$restaurant->city.", ".$restaurant->state." (".$restaurant->zip.")";
//            echo '</td>';
//
//
//            echo '<td>';
//
//            echo "(".substr($restaurant->phone_main, 0, 3).") ".substr($restaurant->phone_main, 3, 3)."-".substr($restaurant->phone_main,6);
//
//            echo '</td>';
//
//
//            echo '</tr>';
//        }

        ?>
<!---->
<!--       </tbody>-->
<!--   </table>-->

 </div>
<div  class="col-md-6 col-xs-12 mapContainer">
    <div id="map_canvas"></div>
</div>
<script src="/resources/js/libs/jquery.dataTables.min.js"></script>
<script src="/resources/js/restaurant-data-table.js"></script>
