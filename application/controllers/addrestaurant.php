<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'third_party/parse/autoload.php');


use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');

class addrestaurant extends MY_Controller {
	
	public function index($renderData=""){	
		
		/*
		 *set up title and keywords (if not the default in custom.php config file will be set) 
		 */
         
         
		$this->title = "Rezzit21";
		$this->keywords = "arny, arnodo";
		
        // 1. when you pass AJAX to renderData it will generate only that particular PAGE skipping other parts like header, nav bar,etc.,
        //      this can be used for AJAX Responses
        // 2. when you pass JSON , then the response will be json object of $this->data.  This can be used for JSON Responses to AJAX Calls.
        // 3. By default full page will be rendered
        
		$this->_render('pages/add-restaurant',$renderData);
	}
	public function create(){
		$account = ParseCloud::run("GetUserByEmail",array("contact"=>$_SESSION['username']));


		$createRestaurant = ParseCloud::run("createRestaurant",
			array(
					"userid"=>$account[0]->getObjectId(),
					"name" => $_POST['name'],
					"phone_main" => $_POST['phone_main'],
					"address_1" => $_POST['address_1'],
					"city" => $_POST['city'],
					"state" => $_POST['state'],
					"zip" => $_POST['zip'],
					"website" => $_POST['website']
			)
		);

		header("location:../snapshot?id=".$createRestaurant['result']->getObjectId());
	}
	public function edit(){
		$account = ParseCloud::run("GetUserByEmail",array("contact"=>$_SESSION['username']));


		$createRestaurant = ParseCloud::run("editRestaurant",
				array(
						"userid"=>$account[0]->getObjectId(),
						"name" => $_POST['name'],
						"phone_main" => $_POST['phone_main'],
						"address_1" => $_POST['address_1'],
						"cuisine" => $_POST['cuisine'],
						"city" => $_POST['city'],
						"state" => $_POST['state'],
						"zip" => $_POST['zip'],
						"website" => $_POST['website'],
						"restId" => $_GET['id']
				)
		);
		header("location:../snapshot?id=".$createRestaurant['result']->getObjectId());
	}
}
