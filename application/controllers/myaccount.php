<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'third_party/parse/autoload.php');


use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');


class myaccount extends MY_Controller {
	
	public function index($renderData=""){

		/*
		 *set up title and keywords (if not the default in custom.php config file will be set)
		 */


		$this->title = "Rezzit21";
		$this->keywords = "arny, arnodo";
		
        // 1. when you pass AJAX to renderData it will generate only that particular PAGE skipping other parts like header, nav bar,etc.,
        //      this can be used for AJAX Responses
        // 2. when you pass JSON , then the response will be json object of $this->data.  This can be used for JSON Responses to AJAX Calls.
        // 3. By default full page will be rendered
//  if($_SESSION['uType'] == "2"){
//	  $this->data['restaurant_sidebar'] = $this->load->view("template/restaurant_sidebar", '', true);
//  }else{
//	  $this->data['diner_sidebar'] = $this->load->view("template/diner_sidebar", '', true);
//  }

               $this->data['states'] = $this->load->view("template/states", '', true);
		$account = ParseCloud::run("GetUserByEmail",array("contact"=>$_SESSION['username']));

		$this->data['uData'] = $account[0];
		$this->_render('pages/my-account',$renderData, false);
	}
	public function editProfile()
	{


		$account = ParseCloud::run("ProfileUpdate",
				array(
						"username" => $_SESSION['username'],
						"title" => $_POST['title'],
						"first_name" => $_POST['first_name'],
						"middle_initial" => $_POST['middle_initial'],
						"last_name" => $_POST['last_name'],
						"suffix" => $_POST['suffix'],
						"birthday_month" => $_POST['birthday_month'],
						"birthday_day" => $_POST['birthday_day'],
						"birthday_year" => $_POST['birthday_year'],
						"anniversary_month" => $_POST['anniversary_month'],
						"anniversary_day" => $_POST['anniversary_day'],
						"anniversary_year" => $_POST['anniversary_year'],
						"address_one" => $_POST['address_one'],
						"address_two" => $_POST['address_two'],
						"city" => $_POST['city'],
						"state" => $_POST['state'],
						"zip" => $_POST['zip'],
						"phone_one" => $_POST['phone_one'],
						"phone_two" => $_POST['phone_two'],
						"mobile" => $_POST['mobile'],
						"fax" => $_POST['fax'],
						"employment_company" => $_POST['employment_company'],
						"employment_position" => $_POST['employment_position'],
						"employment_website" => $_POST['employment_website'],
						"allergies" => $_POST['allergies'],
						"default_home_page" => $_POST['default_home_page']
				)
		);
		header("location:../myaccount");

	}
}
