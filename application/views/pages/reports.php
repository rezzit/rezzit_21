<div class="container">
<?=$restaurant_sidebar?>
  <div class="col-md-6 col-md-pull-3 gridArea">


     <h2>Daily Report</h2>
     <div class="row">
        <div class="col-md-12">
            <h4>Today's Reservations</h4>
            <div class="row">
                <div class="col-md-3">
                    Name
                </div>
                <div class="col-md-3">
                    Email
                </div>
                <div class="col-md-3">
                    Phone
                </div>
                <div class="col-md-2">
                    Date
                </div>
                <div class="col-md-1">
                    Time
                </div>
                <?php
                foreach($myData as $reservation){
                    if(strtotime("today") <= strtotime($reservation->date." ".$reservation->time) && strtotime("tomorrow - 1 second") >= strtotime($reservation->date." ".$reservation->time)){
                        echo '<div class="col-md-3">';
                            echo $reservation->firstName." ".$reservation->lastName;
                        echo '</div>';
                        echo '<div class="col-md-3">';
                            echo $reservation->email;
                        echo '</div>';
                        echo '<div class="col-md-3">';
                            echo $reservation->phone;
                        echo '</div>';
                        echo '<div class="col-md-2">';
                            echo $reservation->date;
                        echo '</div>';
                        echo '<div class="col-md-1">';
                            echo $reservation->time;
                        echo '</div>';

                    }
                }
                ?>
            </div>
        </div>
     </div>
   </div>
 </div> 
