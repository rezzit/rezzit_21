<div class="container">
  <?=$restaurant_sidebar?>
  <div class="col-md-6 col-md-pull-3 gridArea">
  <div class="row">
  <form action="post">
     <h2>Edit Restaurant Staff</h2>
     <div class="row">
        <div class="col-md-6">
           <div class="row">
                <div class="col-md-12">
                   <img height="200" src="https://pixabay.com/static/uploads/photo/2013/07/13/12/07/avatar-159236_640.png">
                   <h3>Name</h3>
                </div>
		<div class="col-md-12">
		   <select name="role"  placeholder="Role">
		       <option value="Owner/Admin">Owner/Admin</option>
		       <option value="General Manager">General Manager</option>
		       <option value="Manager">Manager</option>
		       <option value="Supervisor">Supervisor</option>
		       <option value="General Staff">General Staff</option>
                   </select>
		</div>
		<div class="col-md-12">
		   <input data-validate="required,validPhone" class="phone_mask" name="phone" placeholder="Phone"></input>
		</div>
                <div class="col-md-12">
                   <input data-validate="required,email" name="email"  placeholder="Email"></input>
                </div>
                <div class="col-md-12">
                   <input class="the_date" name="dob" placeholder="Date of birth"></input>
                </div>
                <div class="col-md-12">
                   <button>Save</button>
                </div>
           </div>
        </div>
    </div>
  </form>
  </div>
  <div class="row">
    <form action="post">
      <h2>Add Staff Person</h2>
       <div class="row">
          <div class="col-md-6">Picture: <input type="file" name="new_picture"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required" placeholder="Name" name="new_name"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6">
             <select name="new_role">
               <option value="Owner/Admin">Owner/Admin</option>
               <option value="General Manager">General Manager</option>
               <option value="Manager">Manager</option>
               <option value="Supervisor">Supervisor</option>
               <option value="General Staff">General Staff</option>
             </select>
          </div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required,validPhone" class="phone_mask" placeholder="Phone" name="new_phone"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required,email" placeholder="Email" name="new_email"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input class="the_date" placeholder="Date of birth" name="new_date_of_birth"></input></div>
       </div>
       <div class="row">
          <div class="col-md-12">
               <button type="submit">Save</button>
          </div>
      </div>
    </form>
  </div>
 </div>
</div> 
