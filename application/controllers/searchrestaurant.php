<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require(APPPATH.'third_party/parse/autoload.php');


use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');

class Searchrestaurant extends MY_Controller {
	
	public function index($renderData=""){	
		
		/*
		 *set up title and keywords (if not the default in custom.php config file will be set) 
		 */
         
         
		$this->title = "Rezzit21";
		$this->keywords = "arny, arnodo";
		
        // 1. when you pass AJAX to renderData it will generate only that particular PAGE skipping other parts like header, nav bar,etc.,
        //      this can be used for AJAX Responses
        // 2. when you pass JSON , then the response will be json object of $this->data.  This can be used for JSON Responses to AJAX Calls.
        // 3. By default full page will be rendered


		$list = ParseCloud::run("GetAllRestaurants",array("phrase"=>$_POST['search']));
		$this->data['revProfile'] = $list;
        $_SESSION['time'] = $_POST['hour'];
		$_SESSION['endTime'] = date('h:ia', strtotime($_POST['hour']." + 30 minutes"));   


		$_SESSION['date'] = $_POST['date'];
		$_SESSION['maxSeats'] = $_POST['people'];
		$this->_render('pages/search-restaurant',$renderData, true);
	}
	public function getCurrReservations(){

	}
	public function getAvailSeats(){

		$data = array();
		$list = ParseCloud::run("GetRevCenter",array("restaurant"=>$_GET['id']));
		foreach($list as $revCenter){
			$jsonHour = json_decode($revCenter->hourJSON);
			foreach($jsonHour as $hour){
				if($hour->day == strtolower(date('l'))){
					$l = 0;
					$i = 0;
					$jsonObj = json_decode($revCenter->tableLayout);
					foreach($jsonObj as $seat){
						$l++;
					}


					$avgRez = "60";
					switch($_SESSION['maxSeats']){
						case '3': case '4':
						$avgRez = "90";
						break;
						case '5': case '6':
						$avgRez = "120";
						break;
						case '7': case '8': case '9':
						$avgRez = "120";
						break;
						case '10':
							$avgRez = "135";
							break;
					}


					if(
							strtotime($hour->to) <= strtotime($_GET['time']) &&
							strtotime($hour->from) >= strtotime($_GET['time']." + ".$avgRez." minutes")
					){
//						echo "<hr />";
//						echo "Rez hour to: ";
//						echo date("h:ia", strtotime($hour->to));
//						echo "<br />";
//
//						echo "Rez hour from: ";
//						echo date("h:ia", strtotime($hour->from));
//						echo "<br />";
//						echo "My rez time: ";
//						echo date("h:ia", strtotime($_GET['time']));
//						echo "My rez endTime: ";
//						echo date("h:ia", strtotime($_GET['time']." + ".$avgRez." minutes"));
//						echo "<br />";
						$listt = ParseCloud::run("GetReservationsByRevId",array("revId"=>$revCenter->getObjectId()));
					foreach($listt as $rezCenter){
						if($rezCenter->date == $_SESSION['date']){
							$flag = 0;
							$rezStart = strtotime($rezCenter->time);
							$rezEnd = strtotime($rezCenter->endTime);

							if(strtotime("now") < $rezEnd){
								while ($rezStart < $rezEnd){
									$rezStart = strtotime('+15 minutes',$rezStart);
					//							echo date("h:ia", strtotime(($_GET['endTime']?$_GET['endTime']:$_GET['time']." + 30 minutes")))." - (".date("h:ia", $rezEnd).") ";
					//							echo date("h:ia", $rezStart)."<br />";

									if(strtotime($_GET['time']) <= $rezStart && strtotime($_GET['time']." + ".$avgRez." minutes") > $rezEnd){
										$flag = 1;
										if($_GET['time'] == $rezEnd){
											$flag = 0;
										}
									}
								}

								if($flag == 1){
									$i++;
								}
							}
						}
					}
					$availSeats = 0;
					if($l < $i){
						$availSeats = -1;
					}else{
						$availSeats = $l - $i;
					}
						// IF Day is today
						array_push($data,
								array(
										"objectId"=>$revCenter->getObjectId(),
										"name"=>$revCenter->display_name,
										"avail_seats"=>	$availSeats

								)
						);
					}
				}
				// Loop through rev centers
			}
		}
		echo json_encode($data);

	}
}
