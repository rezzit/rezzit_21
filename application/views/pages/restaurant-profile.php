<div class="container">
  <?=$restaurant_sidebar?>
  <div class="col-md-6 col-md-pull-3">
      <div class="gridArea">
          <h4>Restaurant Details</h4>
      <form method="post"  class="navBit" action="addrestaurant/edit?id=<?=$_GET['id'];?>">
            Restaurant Name
          <input placeholder="Restaurant name" type="text" class="wide" name="name" value="<?=$restaurantDetails->name;?>" required="">
Cuisine
          <div class="styled-select">
          <select name="cuisine" placeholder="Cuisine">
              <?php
              echo '<option selected value="';
              echo $restaurantDetails->cuisine;
              echo '">';
              echo $restaurantDetails->cuisine;
              echo '</option>';
              ?>
              <option value="American">American</option>
              <option value="Italian">Italian</option>
              <option value="Indian">Indian</option>
              <option value="Chinese">Chinese</option>
          </select>
          </div>
Restaurant Phone Number
          <input placeholder="Restaurant phone number"  style="width:160px" type="text" class="wide phone_mask" name="phone_main" value="<?=$restaurantDetails->phone_main;?>" required="">

Restaurant Address
          <input placeholder="Address #1" type="text" class="wide" name="address_1" value="<?=$restaurantDetails->address_1;?>" required="">
Restaurant City
          <input placeholder="City" type="text" class="wide" name="city" value="<?=$restaurantDetails->city;?>" required="">
          Restaurant State
          <div class="styled-select">
          <select name="state" required="" >
              <?php
              echo '<option selected value="';
              echo $restaurantDetails->state;
              echo '">';
              echo $restaurantDetails->state;
              echo '</option>';
              ?>
              <option value="AL">Alabama</option><option value="AK">Alaska</option><option value="AS">American Samoa</option><option value="AZ">Arizona</option><option value="AR">Arkansas</option><option value="CA">California</option><option value="CO">Colorado</option><option value="CT">Connecticut</option><option value="DE">Delaware</option><option value="DC">District Of Columbia</option><option value="FM">Federated States Of Micronesia</option><option value="FL">Florida</option><option value="GA">Georgia</option><option value="GU">Guam</option><option value="HI">Hawaii</option><option value="ID">Idaho</option><option value="IL">Illinois</option><option value="IN">Indiana</option><option value="IA">Iowa</option><option value="KS">Kansas</option><option value="KY">Kentucky</option><option value="LA">Louisiana</option><option value="ME">Maine</option><option value="MH">Marshall Islands</option><option value="MD">Maryland</option><option value="MA">Massachusetts</option><option value="MI">Michigan</option><option value="MN">Minnesota</option><option value="MS">Mississippi</option><option value="MO">Missouri</option><option value="MT">Montana</option><option value="NE">Nebraska</option><option value="NV">Nevada</option><option value="NH">New Hampshire</option><option value="NJ">New Jersey</option><option value="NM">New Mexico</option><option value="NY">New York</option><option value="NC">North Carolina</option><option value="ND">North Dakota</option><option value="MP">Northern Mariana Islands</option><option value="OH">Ohio</option><option value="OK">Oklahoma</option><option value="OR">Oregon</option><option value="PW">Palau</option><option value="PA">Pennsylvania</option><option value="PR">Puerto Rico</option><option value="RI">Rhode Island</option><option value="SC">South Carolina</option><option value="SD">South Dakota</option><option value="TN">Tennessee</option><option value="TX">Texas</option><option value="UT">Utah</option><option value="VT">Vermont</option><option value="VI">Virgin Islands</option><option value="VA">Virginia</option><option value="WA">Washington</option><option value="WV">West Virginia</option><option value="WI">Wisconsin</option><option value="WY">Wyoming</option></select>
              </div>

Restaurant Zip

          <input style="width:160px" placeholder="Zip" type="text" class="wide" name="zip" value="<?=$restaurantDetails->zip;?>" min="5" required="">
Restaurant Website
          <input placeholder="Company website" type="text" class="wide" name="website" value="<?=$restaurantDetails->website;?>">
          Maximum number of people allowed to book online
          <div class="styled-select">
            <select name="maxPpl">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
                <option value="11">11</option>
                <option value="12">12</option>
                <option value="13">13</option>
                <option value="14">14</option>
                <option value="15">15</option>
                <option value="16">16</option>
                <option value="17">17</option>
                <option value="18">18</option>
                <option value="19">19</option>
                <option value="19">19</option>
                <option value="20">20</option>
                <option value="21">21</option>
                <option value="22">22</option>
                <option value="23">23</option>
                <option value="24">24</option>
                <option value="25">25</option>
            </select>
              </div>
          Average time allotted for booking of one person
              <div class="styled-select">
            <select name="avgOne">
                <option value="15">0:15</option>
                <option value="30">0:30</option>
                <option value="45">0:45</option>
                <option value="60">1:00</option>
                <option value="75">1:15</option>
                <option value="90">1:30</option>
                <option value="105">1:45</option>
                <option value="120">2:00</option>
            </select>
              </div>
          Average time allotted for booking of two people
          <div class="styled-select">
            <select name="avgTwo">
                <option value="15">0:15</option>
                <option value="30">0:30</option>
                <option value="45">0:45</option>
                <option value="60">1:00</option>
                <option value="75">1:15</option>
                <option value="90">1:30</option>
                <option value="105">1:45</option>
                <option value="120">2:00</option>
            </select>
          </div>
          Average time allotted for booking of three people
          <div class="styled-select">
            <select name="avgThree">
                <option value="15">0:15</option>
                <option value="30">0:30</option>
                <option value="45">0:45</option>
                <option value="60">1:00</option>
                <option value="75">1:15</option>
                <option value="90">1:30</option>
                <option value="105">1:45</option>
                <option value="120">2:00</option>
            </select>
          </div>
          Average time allotted for booking of four people
          <div class="styled-select">
            <select name="avgFour">
                <option value="15">0:15</option>
                <option value="30">0:30</option>
                <option value="45">0:45</option>
                <option value="60">1:00</option>
                <option value="75">1:15</option>
                <option value="90">1:30</option>
                <option value="105">1:45</option>
                <option value="120">2:00</option>
            </select>
          </div>
          Average time allotted for booking of five people
          <div class="styled-select">
              <select name="avgFive">
                  <option value="15">0:15</option>
                  <option value="30">0:30</option>
                  <option value="45">0:45</option>
                  <option value="60">1:00</option>
                  <option value="75">1:15</option>
                  <option value="90">1:30</option>
                  <option value="105">1:45</option>
                  <option value="120">2:00</option>
              </select>
          </div>
          Average time allotted for booking of 6+ people
          <div class="styled-select">
              <select name="avgSix">
                  <option value="15">0:15</option>
                  <option value="30">0:30</option>
                  <option value="45">0:45</option>
                  <option value="60">1:00</option>
                  <option value="75">1:15</option>
                  <option value="90">1:30</option>
                  <option value="105">1:45</option>
                  <option value="120">2:00</option>
              </select>
          </div>


          <input type="submit" class="button wide" value="Submit">


      </form>
      </div>
  </div>
</div>
