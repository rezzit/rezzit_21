<div class="container">
    <?=$restaurant_sidebar?>
    <?php
    ob_start();
    echo '<div style="display:none;">';
    $hour = intval(date('h')).str_replace('.','',date(':00a'));
    function iterateIntervals($given_hour, $i, $fifteen_intervals, $am_or_pm){
        $selected = '';
        foreach($fifteen_intervals as $interval){
            $hour = $i . ":" . $interval . $am_or_pm;

            echo '<option value="' . $hour .'" ' .$selected . '>' . $hour . '</option>';
            $selected = '';
        }
    }
    $fifteen_intervals = array("00", "15", "30", "45");
    iterateIntervals($hour, "12", $fifteen_intervals, "am");
    for ($i = 1; $i <= 11; $i++)
    {
        iterateIntervals($hour, $i, $fifteen_intervals, "am");
    }
    iterateIntervals($hour, "12", $fifteen_intervals, "pm");
    for ($i = 1; $i <= 11; $i++)
    {
        iterateIntervals($hour, $i, $fifteen_intervals, "pm");
    }
    echo '</div>';
    $out1 = ob_get_contents();
    ?>
    <style>
        .daySelector{
            margin: 20px 0;
        }
        .daySelector span{
            padding: 15px;
            cursor: pointer;
            background-color: #AEAEF7;
            width: 100%;
            display: none;
            font-weight:bold;
            color:white;
        }
        .chosen{
            background-color: #000 !important;
            color:white;
        }

        .dayData{

            /*height: 400px;*/
            /*overflow-y: scroll;*/
        }
        .styled-select{
            margin: 0;
        }
        .gridArea{
            position: relative;
            top:50px;
        }
        #mainContent{
            top: 0;
        }
        .tblHours td{
            font-size:10px;
        }
    </style>
<script>
    $(document).ready(function(){

        // Loop Through All JSON
        var hourContent = '<?=$revProfile->hourJSON?>';
        if(hourContent.replace(/ /g,'')){
            var array = JSON.parse(hourContent);
            array.forEach(function(object) {
                var ref = $('[data-day="'+object.day+'"]').parents(".days").find(".time").last();
                var dest = $(ref).clone().insertBefore($(ref));
                console.log(ref);
                $(dest).find(".to select").val(object.to);
                $(dest).find(".from select").val(object.from);
            });
        }
        var openContent = '<?=$revProfile->openJSON?>';
        if(openContent.replace(/ /g,'')){
            var openArray = JSON.parse(openContent);
            openArray.forEach(function (object) {
                var ref = $('[data-day="' + object.day + '"]').parents(".days").find(".restTime").last();
                var dest = $(ref).clone().insertBefore($(ref));
                console.log(ref);
                $(dest).find(".to select").val(object.to);
                $(dest).find(".from select").val(object.from);
            });
        }
        // END LOOP JSON


        // HIDE ALL STUFF
        $(".days").hide();
        $(".days").find(".time").hide();
        $(".days").find(".restTime").hide();
        // END HIDE

        // SHOW DEFAULT
        $('[data-day="sunday"]').parents(".days").show().find(".restTime").show();
        $('.restHours [data-date="sunday"]').addClass('chosen');
        // END SHOW DEFAULT

        $('.restHours td').click(function(){
            $('.days').hide();
            $(".days").find(".time").hide();
            $(".days").find(".restTime").hide();
            $("[data-day='"+$(this).attr('data-date')+"']").parents(".days").show();
            $("[data-day='"+$(this).attr('data-date')+"']").parents(".days").find(".restTime").show();

            $(".chosen").removeClass("chosen");
            $(this).addClass("chosen");
        });
        $('.rezHours td').click(function(){
            $('.days').hide();
            $(".days").find(".time").hide();
            $(".days").find(".restTime").hide();
            $("[data-day='"+$(this).attr('data-date')+"']").parents(".days").show();
            $("[data-day='"+$(this).attr('data-date')+"']").parents(".days").find(".time").show();

            $(".chosen").removeClass("chosen");
            $(this).addClass("chosen");
        });
        $(".restTime .addARow").click(function(){
            var ref = $(this).parents(".days").find(".restTime").last();
            $(ref).clone().insertAfter($(this).parents('.restTime'));
        });
        $(".time .addARow").click(function(){
            var ref = $(this).parents(".days").find(".time").last();
            $(ref).clone().insertAfter($(this).parents('.time'));
        });
        $("select").change(function(){
            var arr = [];
            var openArr = [];
           $(".days").each(function(){
               var day = $(this).find(".daySelector span").attr('data-day');
               $(this).find(".time").each(function(){
                   var to = $(this).find(".to select").val();
                   var from = $(this).find(".from select").val();
                   if(to !== "(null)" && from !== "(null)"){
                       arr.push({
                           "day":day,
                           "to":to,
                           "from":from
                       });
                   }
               });
               $(this).find(".restTime").each(function(){
                   var to = $(this).find(".to select").val();
                   var from = $(this).find(".from select").val();
                   if(to !== "(null)" && from !== "(null)"){
                       openArr.push({
                           "day":day,
                           "to":to,
                           "from":from
                       });
                   }
               });
           });


            $("#dayTimes").text(JSON.stringify(arr));
            $("#openTimes").text(JSON.stringify(openArr));
            console.log(openArr);
        });
    });
</script>
    <div class="col-md-6 col-md-pull-3 gridArea" id="mainContent">
  <form action="../../revCenter/processRevData?id=<?php echo $_GET['id'];?><?php echo ($_GET['revId'])?'&revId='.$_GET['revId']:null?>" method="post">
     <h2><?php echo ($revProfile->display_name)? $revProfile->display_name : "Add" ;?> Revenue Center</h2>


      <div class="row">
          <div class="col-md-12">
              <h3>Revenue Center Name</h3>
          </div>
          <div class="col-md-12">
              <input name="display_name" placeholder="Display Name" value="<?=$revProfile->display_name;?>">
          </div>
      </div>
      <div class="row">
          <div class="col-md-12">
              <h3>Revenue Center Description</h3>
          </div>
          <div class="col-md-12">
              <input name="description"  placeholder="<?=$revProfile->display_name;?> description here..." value="<?=$revProfile->description;?>">
          </div>
      </div>
<!--      <div class="row">-->
<!--          <div class="col-md-12">-->
<!--              <h3>Revenue Center Cuisine</h3>-->
<!--          </div>-->
<!--          <div class="col-md-12">-->
<!--              <select name="cuisine" placeholder="Cuisine">-->
<!--                  --><?php
//                  echo '<option selected value="';
//                  echo $revProfile->cuisine;
//                  echo '">';
//                  echo $revProfile->cuisine;
//                  echo '</option>';
//                  ?>
<!--                  <option value="American">American</option>-->
<!--                  <option value="Italian">Italian</option>-->
<!--                  <option value="Indian">Indian</option>-->
<!--                  <option value="Chinese">Chinese</option>-->
<!--              </select>-->
<!--          </div>-->
<!--      </div>-->
      <div class="row">
          <div class="col-md-12">
              <h3>Revenue Center Pricing</h3>
          </div>
          <div class="col-md-12">
              <input name="price" placeholder="$$$" value="<?=$revProfile->price;?>">
          </div>
      </div>
    <div class="row">
        <h3>Amenities</h3>
        <select multiple name="benefits[]">
            <?php
            foreach($revProfile->benefits as $benefits){
                echo '<option selected value="';
                echo $benefits;
                echo '">';
                echo $benefits;
                echo '</option>';
            }
            ?>
          <option value="Valet Parking">Valet Parking</option>
          <option value="On-Site Parking">On-Site Parking</option>
          <option value="Elevator">Elevator</option>
          <option value="Live Entertainment">Live Entertainment</option>
          <option value="Roof Deck">Roof Deck</option>
          <option value="Patio">Patio</option>
        </select>
    </div>
    <div class="row">
        <h3>Public Transportation</h3>
        <textarea name="transport" placeholder="Enter directions here."><?=$revProfile->transport;?></textarea>
    </div>
    <div class="row">
        <h3>Dress Code</h3>
        <select multiple name="dressCode[]">
            <?php
                foreach($revProfile->dressCode as $dressCode){
                    echo '<option selected value="';
                        echo $dressCode;
                    echo '">';
                        echo $dressCode;
                    echo '</option>';
                }
            ?>
          <option value="casual">Casual</option>
          <option value="business casual">Business Casual</option>
          <option value="casual elegant">Casual Elegant</option>
          <option value="formal">Formal</option>
          <option value="jacket required">Jacket Required</option>
        </select>
    </div>
      <div class="row">
          <div class="col-md-12">
              <h3>Revenue Center Hours</h3>
          </div>
          <div class="col-md-12 tblHours">
              <table class="table table-responsive">
                  <tr>
                      <td>Sunday</td>
                      <td>Monday</td>
                      <td>Tuesday</td>
                      <td>Wednesday</td>
                      <td>Thursday</td>
                      <td>Friday</td>
                      <td>Saturday</td>
                  </tr>
                  <tr class="restHours">
                      <td data-date="sunday">Restaurant Hours</td>
                      <td data-date="monday">Restaurant Hours</td>
                      <td data-date="tuesday">Restaurant Hours</td>
                      <td data-date="wednesday">Restaurant Hours</td>
                      <td data-date="thursday">Restaurant Hours</td>
                      <td data-date="friday">Restaurant Hours</td>
                      <td data-date="saturday">Restaurant Hours</td>
                  </tr>
                  <tr class="rezHours">
                      <td data-date="sunday">Reservation Hours</td>
                      <td data-date="monday">Reservation Hours</td>
                      <td data-date="tuesday">Reservation Hours</td>
                      <td data-date="wednesday">Reservation Hours</td>
                      <td data-date="thursday">Reservation Hours</td>
                      <td data-date="friday">Reservation Hours</td>
                      <td data-date="saturday">Reservation Hours</td>
                  </tr>
              </table>
          </div>
      </div>
      <?php date_default_timezone_set('US/Eastern');?>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="sunday">Sunday</span>
          </div>
          <div class="col-md-12 dayData"> 
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="monday">Monday</span>
          </div>
          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="tuesday">Tuesday</span>
          </div>
          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="wednesday">Wednesday</span>
          </div>
          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="thursday">Thursday</span>
          </div>
          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="friday">Friday</span>
          </div>
          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>
      <div class="row days">
          <div class="col-md-12 daySelector">
              <span data-day="saturday">Saturday</span>
          </div>

          <div class="col-md-12 dayData">
              <div class="row">
                  <div class="col-md-6">
                      From
                  </div>
                  <div class="col-md-6">
                      To
                  </div>
              </div>
              <div class="row restTime">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
              <div class="row time">
                  <div class="col-md-5 styled-select to">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-5 styled-select from">
                      <select>
                          <?php
                          echo '<option selected value="(null)"> - </option>';
                          echo $out1;
                          ?>
                      </select>
                  </div>
                  <div class="col-md-2 addARow btn btn-primary">
                      Add Row <span class="glyphicon glyphicon-plus"></span>
                  </div>
              </div>
          </div>
      </div>



      <textarea id="dayTimes" name="hourJSON" style="display: none;"></textarea>
      <textarea id="openTimes" name="openTimes" style="display: none;"></textarea>

      <div class="row">
          <div class="col-md-12">
               <button>Save</button>
           </div>
      </div>
    </div>
  </form>
  </div>
</div>

