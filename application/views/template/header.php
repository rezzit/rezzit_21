
<div class="container-fluid navbar-fixed-top topNav">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-6" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
            </button>
            <a class="navbar-brand" href="/"><img src="../../resources/img/rezzit21-logo.png" class="img-responsive" ></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
            <?php echo $nav?>
        </div><!-- /.navbar-collapse -->

</div>
