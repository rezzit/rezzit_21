<?php
require(APPPATH.'third_party/parse/autoload.php');

use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');
?>
<style>
    #main{
        background: white;
    }
    h4{
        margin-top: -5px;
    }
    .gridArea{
        border: thin solid #ccc;
        background: #dcdcdc;
        text-align: center;
        margin-top: 20px;
        font-size: 12px;
        padding: 20px;
    }
    .navBit{
        border-top: thin solid #ccc;
        padding: 0 20px;
        padding-top: 20px;
        margin: auto -20px;
        margin-bottom: 20px;
    }
    .gridArea img{
        border-radius: 100px;
        width: 200px;
        height: 200px;
        margin: auto;
        display: block;
    }
    .styled-select select {
        background: transparent;
        font-size: 12px;
        line-height: 1;
        border: 0;
        border-radius: 0;
        -webkit-appearance: none;
        color:black;
    }
    .styled-select {
        padding: 0;
        padding-right: 32px;
        overflow: hidden;
        background: #fff url("http://www.scottgood.com/jsg/blog.nsf/images/arrowdown.gif") no-repeat 98% 50%;
        border: 1px solid #ccc;
        /* background-position: center right; */
        margin: 3px;
        width: 100%;
    }
    input,select{
        width: 100% !important;
        display: block !important;
        padding: 10px;
        margin: 3px;
    }
</style>

<div class="col-md-3">

        <div class="gridArea">
<img width="100%" src="https://pixabay.com/static/uploads/photo/2013/07/13/12/07/avatar-159236_640.png">
<?php
$account = ParseCloud::run("GetUserByEmail",array("contact"=>$_SESSION['username']));

$restaurantDetails = ParseCloud::run("GetRestaurant",
    array(
        "userid"=>$account[0]->getObjectId(),
        "restaurant"=>$_GET['id']
    )
)[0];
?>
    <h3><?=$restaurantDetails->name;?></h3>
<ul class="nav nav-sidebar">
  <li><a href="/snapshot?id=<?=$_GET['id'];?>">Snapshot</a></li>
  <li><a href="/reports?id=<?=$_GET['id'];?>">Reports</a></li>
  <!--<li><a href="/preorders">Preorders</a></li>-->
  <li><a href="/restaurantstaff?id=<?=$_GET['id'];?>">Staff</a></li>
  <li><a href="/restaurantvendor?id=<?=$_GET['id'];?>">Vendors</a></li>
  <li><a href="/restaurantprofile?id=<?=$_GET['id'];?>">Restaurant Profile</a></li>
</ul>
</div>
</div>
<div class="col-md-3 col-md-push-6">
    <div class="gridArea">
        <h4>Revenue center management</h4>

        <div  class="navBit" >
            <a href="revCenter?id=<?php echo $_GET['id'];?>">New Revenue Center</a>
        </div>
        <?php
        $list = ParseCloud::run("GetRevCenter",array("restaurant"=>$_GET['id']));
        //        var_dump($list);
        foreach($list as $revCenter){
            echo '<div class="navBit">';
            echo $revCenter->display_name;
            echo '<br />';
            echo '<a href="/revCenter/editRevCenter?id=';
            echo $_GET['id'];
            echo '&revId=';
            echo $revCenter->getObjectId();
            echo '">';
            echo 'Edit';
            echo '</a>';
            echo ' | ';
            echo '<a href="/tableBuilder?id=';
            echo $_GET['id'];
            echo '&revId=';
            echo $revCenter->getObjectId();
            echo '">';
            echo 'Table Builder';
            echo '</a></div>';

        }
        ?>
    </div>

        </div>
