
<!doctype html> <html> <head> <meta charset="utf-8"> <title>VetX</title> <meta name="description" content="VetX"> <meta name="author" content="Craftr"> <meta name="viewport" content="width=device-width,initial-scale=1"> <!-- Place favicon.ico and apple-touch-icon.png in the root directory --> <link rel="stylesheet" href="styles/vendor.be294c9a.css"> <link rel="stylesheet" href="styles/main.dc46616a.css"> <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> <link rel="icon" type="image/png" href="favicon.png"> </head>
<body ng-app="vetxWebappApp" ng-controller="AppCtrl" class="ng-scope" cz-shortcut-listen="true"> <!--[if lte IE 8]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]--> <!-- Add your site or application content here --> <div class="header-container" style="display: none;">




    <header class="grid-container">
        <div id="right" class="grid-50 mobile-grid-100 ng-hide" ng-show="isLoggedIn">
            <br>
            <img class="logo" src="http://vet.vetxapp.com/images/logo.74cf7a46.png">
            <img class="uProfile" src="http://files.parsetfss.com/20fcc3fb-9d89-49ce-898b-b0ef7d1743f3/tfss-b2a5f6ae-896a-4276-a924-c34c3a5242b9-image">

            <span id="icons">
                <a href="#/dashboard">Unassigned <span class="arrow">&gt;</span></a>
                <a href="#/active">Active <span class="arrow">&gt;</span></a>

                <a href="#/profile">Profile <span class="arrow">&gt;</span></a>
                <a href="#/logout">Logout <span class="arrow">&gt;</span></a>




            </span>
        </div>
    </header>

</div><div id="user" class="ng-binding"></div> <!-- <div class="header">
    <a href="#/earnings"><li>Earnings</li></a>
      <div class="navbar navbar-default" role="navigation">
        <div class="container">
          <div class="navbar-header">

            <a class="navbar-brand" href="#/">vetxWebapp</a>
          </div>

          <div class="collapse">

            <ul class="nav navbar-nav">
              <li class="active"><a href="#/">Home</a></li>
              <li><a ng-href="#/about">About</a></li>
              <li><a ng-href="#/">Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div> --> <div class="container body"> <!-- ngView:  --><div ng-view="" class="ng-scope"><div id="wrapper" class="grid-container col ng-scope"> <div id="left-panel" class="blocky grid-50 hide-on-mobile"><img class="logop" src="http://vet.vetxapp.com/images/logo.74cf7a46.png"></div> <div id="right-panel" class="blocky grid-50 mobile-grid-100"> <form name="loginForm" novalidate="" ng-submit="loginForm.$valid &amp;&amp; doLogin()" class="ng-valid-email ng-valid-minlength ng-valid-maxlength ng-dirty ng-valid-parse ng-valid ng-valid-required"> <input class="inputBlock ng-untouched ng-valid-email ng-dirty ng-valid ng-valid-required" type="email" name="email" placeholder="Email" ng-model="userCredentials.username" required=""> <!-- ngIf: loginForm.email.$touched --> <input type="password" name="password" class="inputBlock ng-untouched ng-valid-minlength ng-valid-maxlength ng-dirty ng-valid-parse ng-valid ng-valid-required" placeholder="Password" ng-model="userCredentials.password" minlength="6" maxlength="18" required=""> <!-- ngIf: loginForm.password.$touched --> <input type="submit" class="sButton" value="LOGIN" style="margin-top: 40px" ng-disabled="loginForm.$invalid"> </form> <!-- <a href="#"><i class="fa fa-user"></i>Create your account</a> --> </div> </div></div> </div> <div growl="" class="ng-isolate-scope"><div class="growl-container growl-fixed bottom-center" ng-class="wrapperClasses()"><!-- ngRepeat: message in growlMessages.directives[referenceId].messages --></div></div> <!-- <div class="footer">
      <div class="container">
        <p><span class="glyphicon glyphicon-heart"></span> from the Yeoman team</p>
      </div>
    </div> --> <!-- Google Analytics: change UA-XXXXX-X to be your site's ID --> <script src="https://www.google-analytics.com/analytics.js"></script><script>!function(A,n,g,u,l,a,r){A.GoogleAnalyticsObject=l,A[l]=A[l]||function(){
            (A[l].q=A[l].q||[]).push(arguments)},A[l].l=+new Date,a=n.createElement(g),
        r=n.getElementsByTagName(g)[0],a.src=u,r.parentNode.insertBefore(a,r)
    }(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');</script> <script src="//www.parsecdn.com/js/parse-1.6.5.min.js"></script><script>       Parse.initialize("F2ORC4KCdlA91I9tKV2oZJycM8pZPttjS94tMZDH", "RLs0IarADM78Vbg8Ejl4XB81Z1iQLfYUdNTNTIr1");</script>

<style>
    #right .logo{
        width: 60%;
    }
    #right .uProfile{
        display: none;
    }
    #submission{
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-color: white;
        z-index: 10000 !important;
        padding: 15% 20%;
        text-align: center;
        display: none;
    }
    #submission textarea{
        width: 100%;
        height: 300px;
    }
    #closeMe{
        background: red;
        padding: 10px;
        border-radius: 100px;
        display: inline-block;
        color: white;
        width: 40px;
        position: relative;
        left: 50%;
    }
    #openReport{
        position: absolute;
        bottom: 5px;
        right: 0;
    }
    .grid-20 img{
        width: 128px;
        height: 128px;
        border-radius: 100px;
    }
    #icons{
        margin-top: 40px;
    }
</style>

<script>


    $( document ).ready(function() {

        if(!$("#user").html()){
            $(".header-container").css({
                "display":"none"
            })
        }

        if(window.location == "http://vet.vetxapp.com"){
            window.location.href = "http://vet.vetxapp.com/#/";
        }

        if(window.location == "http://vet.vetxapp.com/#/" && $("#user").html()){
            window.location.href = "http://vet.vetxapp.com/#/active";
        }

        if(window.location == "http://vet.vetxapp.com/#/login" && $("#user").html()){
            window.location.href = "http://vet.vetxapp.com/#/active";
        }


        $( "body" ).mousemove(function( event ) {


//.css({
//                "position": "relative",
//                        "left": "-55px"
//            })
            if(!$("#user").html()){
                $(".header-container").css({
                    "display":"none"
                })
            }

            if($("#user").html()){
                $(".header-container").css({
                    "display":"inline-block"
                })
            }
            $("#chat .uProfile").remove()
            $(".right").after($("#leftt-panel").find("img").clone());
            $(".left").before($("#right").find(".uProfile").clone());

            if(window.location == "http://vet.vetxapp.com/#/" && $("#user").html()){
                window.location.href = "http://vet.vetxapp.com/#/active";
            }

            if(window.location == "http://vet.vetxapp.com/#/login" && $("#user").html()){
                window.location.href = "http://vet.vetxapp.com/#/active";
            }
            if(sessionStorage.getItem("reset") == 1){
                sessionStorage.reset = 0;
                location.reload();
            }
            if($(".grid-80").length == 0){
                $(".alert-panel").html("You currently have no open consultations.");
            }else{
                $(".alert-panel").html(" ");
            }
            var html = "";
            if(sessionStorage.getItem("oldReport").indexOf("Veterinarian") > -1){

                html = "It appears that a report has already been generated for this consultation, you may view it anytime <a style='color:lightblue;text-decoration:underline;' target='_blank' href='"+sessionStorage.getItem("oldReport")+"'>by visiting this link.</a><br /><br />";



            }

            var xyz = 0;
            $('.bubble:contains("http")').each(function () {
                html += '<a target="_blank" href="'+$(this).html()+'"><div>Media Link '+xyz+'</div></a>';
                xyz++;
            });
            $(".media-panel").html(html);
        });
        var data;


        $('a[href="#/dashboard"]').find(".unread").html('<img src="Alert.png" style="height: 27px;">');





        $('.closeMe').click(function(){
            $(this).parents(".genForm").hide();
        });
        $('.openReport').click(function(){
            $(".genForm").show();
        });


        $("#genBtn").click(function(){
            $.ajax({
                type: "POST",
                url: "reply.php",
                data: {
                    From: sessionStorage.getItem("docPhone"),
                    To: sessionStorage.getItem("questionPhone"),
                    Body: "["+$("#infoprov").val()+"]"
                },

                success: function(data, textStatus, jqXHR)
                {
                    window.location.href = "http://vet.vetxapp.com/#/active";
                }
            });

        });



    });
</script>
</body>
</html>