<style>

    .row .modBlock{
        text-align: center;
        margin: auto;
        margin-bottom: 20px;
    }
    .restBlock{
        padding: 20px;
        background: #999;
        border-top: thin solid #000;
        border-bottom: thin solid #000;
    }
    .arrow{
        font-size: 42px;
        color: #444;
    }
    a{
        color:#333;
        font-weight: bold;
    }
</style>
<div class="container">

    <div class="row">
        <div class="col-md-12 modBlock">
            <h2 style="text-align: center;">Manage my restaurants</h2>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12 modBlock">
            <a style="padding:10px 20px;border-radius: 5px;background: #888;color:white;text-decoration: none;" href="addrestaurant">Add a new restaurant <span class="glyphicon glyphicon-plus"></span></a>
        </div>
    </div>
    <?php
        foreach($myData as $restaurant){
            echo '<div class="row">';
                echo '<a href="snapshot?id='.$restaurant->getObjectId().'">';
                echo '<div class="col-md-11 restBlock">';
                    echo "<div>";
                    echo $restaurant->address_1;
                    echo " - ";
                    echo $restaurant->city;
                    echo ", ";
                    echo $restaurant->state;
                    echo " (";
                    echo $restaurant->zip;
                    echo ") ";
                    echo "</div>";
                    echo $restaurant->name;
                    echo "<div>";
                    echo $restaurant->phone_main;
                    echo "</div>";
                echo '</div>';
                echo '<div class="arrow col-md-1 restBlock">></div></a>';
            echo '</div>';
        }
    ?>
</div>
