$(document).ready(function(){
   jQuery("input.phone_mask").mask('(000) 000-0000');
   $.verify.addRules({
      validPhone: function(r) {
         return r.val().match(/\d/g).length===10 ? true : "10 digits please";
      }, 
     confirmPassword: function(r){
        return $("[name='password']").val() === r.val() ? true : "Passwords must match";
     }
   });
});
