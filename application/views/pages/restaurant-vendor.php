<div class="container">
  <?=$restaurant_sidebar?>
  <div class="col-md-6 col-md-pull-3 gridArea">
  <div class="row">
  <form action="post">
     <h2>Restaurant Vendor</h2>
     <div class="row">
        <div class="col-md-6">
           <div class="row">
		<div class="col-md-12">
		   <input data-validate="required" name="name"  placeholder="Name"></input>
		</div>
           </div>
           <div class="row">
		<div class="col-md-12">
		   <input data-validate="required" name="contact" placeholder="Contact Name"></input>
		</div>
           </div>
           <div class="row">
                <div class="col-md-12">
                   <input data-validate="required,email" name="email"  placeholder="Email"></input>
                </div>
           </div>
           <div class="row">
                <div class="col-md-12">
                   <input data-validate="required" name="address" placeholder="Address"></input>
                </div>
           </div>
           <div class="row">
                <div class="col-md-4">
                   <input data-validate="required" name="city" placeholder="City"></input>
                </div>
                <div class="col-md-4">
                   <select name="state" placeholder="State">
                      <?=$states?> 
                   </select>
                </div>
                <div class="col-md-4">
                   <input data-validate="required,number,min(5)" name="zip" placeholder="Zip"></input>
                </div>
           </div>
           <div class="row">
                <div class="col-md-12">
                   <input data-validate="url" name="website" placeholder="Website"></input>
                </div>
           </div>
        </div>
        <div class="col-md-3">
           Delivery Schedule
           <select name="delivery_schedule" multiple>
                <option value="Sunday">Sunday</option>
                <option value="Monday">Monday</option>
                <option value="Tuesday">Tuesday</option>
                <option value="Wednesday">Wednesday</option>
                <option value="Thursday">Thursday</option>
                <option value="Friday">Friday</option>
                <option value="Saturday">Saturday</option>
           </select>
        </div>
        <div class="col-md-3">
            <div class="row">
              <div class="col-md-12">
                 <input data-validate="required,validPhone" class="phone_mask" name="phone_one" placeholder="Phone #1">
              </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                 <input data-validate="validPhone" class="phone_mask" name="phone_two" placeholder="Phone #2">
               </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                <input data-validate="validPhone" class="phone_mask" name="fax" placeholder="Fax">
                </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                <label><input type="checkbox" name="active"> Active</label>
               </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit">Update</button>
            </div>
        </div>
    </div>
  </form>
  </div>
  <div class="row">
    <form action="post">
      <h2>Add Vendor</h2>
       <div class="row">
          <div class="col-md-6"><input data-validate="required"  name="new_name" placeholder="Vendor Name"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required" name="new_name" placeholder="Contact Name"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required,validPhone" class="phone_mask" name="new_phone" placeholder="Phone"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required,email" name="new_email" placeholder="Email"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required" name="new_address" placeholder="Address"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required" name="new_city" placeholder="City"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><select name="new_state" placeholder="State"><?=$states?></select></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="required,number,min(5)" name="new_zip" placeholder="Zip"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="url" name="new_website" placeholder="Website"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="validPhone" class="phone_mask" name="new_phone_one" placeholder="Phone #1"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="validPhone" class="phone_mask" name="new_phone_two" placeholder="Phone #2"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><input data-validate="validPhone" class="phone_mask" name="new_fax" placeholder="Fax"></input></div>
       </div>
       <div class="row">
          <div class="col-md-6"><button type="submit">Add</button></div>
       </div>
    </form>
  </div>
 </div>
</div> 
