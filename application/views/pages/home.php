<style>
  body,html{
    width: 100%;
    overflow-x: hidden;
  }
  #main{
    padding:0 !important;
  }
  a{
    text-decoration: none !important;
  }
  #headerContainer{
    background:url(resources/img/perrys-rotator-pe-20140415-1600x500.png);
    margin: 0;
    width: 100%;
    color: white;
    padding: 1% 1%;
    text-align: center;
    background-attachment: fixed;
    background-position: top left;
    background-size: cover;
  }
  .heroArea{
    padding: 10% 0 10% 0;
  }
  .heroArea input{
    padding:19px;
    color: #000;
  }
  .secondHero{
    background:url(resources/img/secondImg.png);
    margin: 0;
    width: 100%;
    color: white;
    padding: 1% 1%;
    text-align: center;
    background-attachment: fixed;
    background-position: top left;
    background-size: cover;
  }
  .secondHero h2{
    border: 6px solid white;
    padding: 40px 20px;
    width: 60%;
    font-size: 45px;
    height: 250px;
  }

  .secondHero .description{
    background: teal;
    width: 30%;
    padding: 30px;
    font-size: 18px;
    float: right;
    text-align: left;
    position: relative;
    top:40px;
    right: 5%;
  }
  .cta{
    color: white;
    text-align: center;
    background: #5ec1d8;
    width: 100%;
    padding: 5% 5% 0% 5%;
  }
  .cta h2{
    font-size: 50px;
    margin-bottom: 30px;
  }
  .thirdHero{
    background:url(resources/img/thirdHero.png);
    margin: 0;
    width: 100%;
    color: white;
    padding: 1% 1%;
    text-align: center;
    background-attachment: fixed;
    background-position: top left;
    background-size: cover;
  }

  .thirdHero h2{
    border: 6px solid white;
    padding: 40px 20px;
    width: 60%;
    font-size: 45px;
    height: 250px;
    float: right;
  }

  .thirdHero .description{
    background: teal;
    width: 30%;
    padding: 30px;
    font-size: 18px;

    text-align: left;
    position: relative;
    top:-40px;
    left: 5%;
    clear: both;
  }
  .navi span{
    margin: 12px;
    font-size: 14px;
  }
  .navi span a{
    color:white;
  }
  .signupBtn{
    font-size: 17px;
    color: #5ec1d8 !important;
    border: 2px solid #5ec1d8;
    padding: 10px 15px;
    border-radius: 3px;
  }
  #bNavi{
    padding: 15px 0;
  }
  #bNavi img, #bNavi span{
    position: relative;
    top: 5px;
    left: 10px;
  }


  .bNavi span{
    margin: 12px;
    font-size: 16px;
  }
  .bNavi span a{
    color:black;
  }

  footer{
    padding: 0px;
    clear: both;
    height: 55px;
  }

  /*COMMON CSS*/


  #regForm form{
    margin: 30px 0;
  }
  .styled-select select {
    background: transparent;
    font-size: 16px;
    line-height: 1;
    border: 0;
    border-radius: 0;
    -webkit-appearance: none;
    color:black;
  }
  .styled-select {
    padding: 20px 10px;
    overflow: hidden;
    background: #fff url("http://www.scottgood.com/jsg/blog.nsf/images/arrowdown.gif") no-repeat 90% 50%;
    border: 1px solid #ccc;
  }
  #regForm .col-md-3, #regForm .col-md-4, #regForm .col-md-1{
    padding: 0;
  }
  #regForm button{
    background: #5ec1d8;
    color: white;
    padding: 15px 20px;
    border: none;
  }
  .floater{
    margin: 50px 0;
    background: none;
    color: white;
    padding: 10px 20px;
  }
  #regForm .btn-default{
    color: #ffffff;
     background: none;
     border: none;
  }

  .description .btn-default{
    display: block;
    margin: 0px auto;
    margin-top: 20px;
  }


  @media (max-width: 800px) {
    .secondHero h2{
      border: 6px solid white;
      padding: 40px 20px;
      height: auto;
      width: 100%;
      font-size: 45px;
    }
    .secondHero .description{
      background: teal;
      width: 100%;
      padding: 30px;
      font-size: 18px;
      text-align: center;
      position: static;
      float: none;
    }


    .thirdHero h2{
      border: 6px solid white;
      padding: 40px 20px;
      width: 100%;
      font-size: 45px;
      position: static;
      height: auto;
      float: none;
    }

    .thirdHero .description{
      background: teal;
      width: 100%;
      padding: 30px;
      font-size: 18px;

      text-align: center;
      clear: both;
      position: static;
      float: none;
    }
    #regForm button{
      width: 100%;
    }
  }
</style>
<script>
  $(document).ready(function(){
    $('nav').hide();
    $('.topNav').hide();
    $('#stickyLogo').hide();
    $('form[action="searchrestaurant"]').appendTo('#regForm');
    $('.signupBtn').click(function(){
      $('#myModal').modal('show');
    });
    $('#submitSignup').click(function(){
      $.ajax({
        type: "POST",
        url: "signup/createSignup",
        data: {
          firstname: $('#myModal input[name="firstname"]').val(),
          lastname: $('#myModal input[name="lastname"]').val(),
          email: $('#myModal input[name="email"]').val(),
          password: $('#myModal input[name="password"]').val()
        },

        success: function(data, textStatus, jqXHR)
        {
          location.reload();
        }
      });
    });
    $('.loginBtn').click(function(){
      $('#myModalTwo').modal('show');
    });
    $('#submitLogin').click(function(){
      $.ajax({
        type: "POST",
        url: "login/signin",
        data: {
          email: $('#myModalTwo input[name="email"]').val(),
          password: $('#myModalTwo input[name="password"]').val()
        },

        success: function(data, textStatus, jqXHR)
        {
          location.reload();
        }
      });
    });
  });
  $(document).scroll(function()
    {
    if($(window).scrollTop() === 0) {
      $(".menu").hide();
    }else{
      $(".menu").show();
    }
  });
</script>
<div id="headerContainer" class="container">
  <div class="row">
    <div class="col-md-2">
      <img src="resources/img/rez21White.png" class="img-responsive" />
    </div>
    <div class="navi col-md-5 col-md-offset-5">
      <span>
        <a href="#features">
          Features
        </a>
      </span>
            <span>
        <a href="#restaurant">
          Restaurant Owners
        </a>
      </span>
            <span>
        <a href="#" class="loginBtn">
          Login
        </a>
      </span>
            <span>
        <a href="#" class="signupBtn">
          Sign Up
        </a>
      </span>
    </div>
  </div>

  <div class="row heroArea">
    <div class="col-md-12">

      <h1>Define Your Dining Experience</h1>
      <div id="regForm"></div>
    </div>
  </div>
 </div>
<div class="row" id="bNavi">
  <div class="col-md-2">
    <img src="resources/img/rezzit21-logo.png" class="img-responsive" />
  </div>
  <div class="bNavi col-md-2 col-md-offset-8">
            <span>
        <a href="Features">
          Login
        </a>
      </span>
            <span>
        <a href="Features">
          Sign Up
        </a>
      </span>
  </div>
</div>
<div class="container  secondHero" id="features">
  <div class="row">
    <div class="col-md-12">

      <h2>TAKE CONTROL OF YOUR ENTIRE EXPERIENCE</h2>
      <div class="description">
        Rezzit21's state of the art reservation process gives you a full interactive way to easily plan your entire dining experience from any device, at any time. It's more than just a reservation.
        <button class="btn-default btn ghost floater">Get Started</button>
      </div>

    </div>
  </div>
  </div>
  <div class="container  cta" id="diner">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">

      <h2>Define Your Dining Experience</h2>
      <p>
        Rezzit21 allows you to pre-order anything from appetizers, decanted wine or special occasion flowers, to premium seating, and even extra time at your favorite table, and pay for those pre-ordered amenities ahead of time. Best of all? There are no booking or hidden fees. Rezzit21 is the dining app you've been waiting for.
      </p>
      <button class="btn-default btn ghost floater">More Info</button>
    </div>
  </div>
</div>
<div class="container  thirdHero" id="restaurant">
  <div class="row">
    <div class="col-md-12">

      <h2>TAKE CONTROL OF YOUR ENTIRE EXPERIENCE</h2>
      <div class="description">
        Rezzit21's state of the art reservation process gives you a full interactive way to easily plan your entire dining experience from any device, at any time. It's more than just a reservation.
        <button class="btn-default btn ghost floater">Get Started</button>
      </div>

    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Account Signup</h4>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">
            <div class="col-md-12">
              <input data-validate="required" name="firstName" placeholder="First Name">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input data-validate="required" name="lastName" placeholder="Last Name">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input data-validate="required,email" name="email" placeholder="Email">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="password" data-validate="required,min(12)" name="password" placeholder="Password">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="password" data-validate="confirmPassword" name="confirm_password" placeholder="Confirm password">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="checkbox">I agree to Rezzit<sup>21</sup> Terms &amp; Conditions
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="submitSignup" type="button" class="btn btn-primary">Signup</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModalTwo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Account Login</h4>
      </div>
      <form>
      <div class="modal-body">

          <div class="row">
            <div class="col-md-12">
              <input data-validate="required,email" name="email" placeholder="Email">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <input type="password" data-validate="required,min(12)" name="password" placeholder="Password">
            </div>
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="submitLogin" type="button" class="btn btn-primary">Login</button>
      </div>
      </form>
    </div>
  </div>
</div>