<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'third_party/parse/autoload.php');


use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');

class tableBuilder extends MY_Controller {

    public function index($renderData=""){

        /*
         *set up title and keywords (if not the default in custom.php config file will be set)
         */


        $this->title = "Rezzit21";
        $this->keywords = "arny, arnodo";

        // 1. when you pass AJAX to renderData it will generate only that particular PAGE skipping other parts like header, nav bar,etc.,
        //      this can be used for AJAX Responses
        // 2. when you pass JSON , then the response will be json object of $this->data.  This can be used for JSON Responses to AJAX Calls.
        // 3. By default full page will be rendered

        $list = ParseCloud::run("GetRevCenter",array("restaurant"=>$_GET['id']));
        foreach($list as $revCenter){
            if($revCenter->getObjectId() == $_GET['revId']){
                $this->data['tableLayout'] = $revCenter->tableLayout;
                $this->data['chairLayout'] = $revCenter->chairLayout;
            }
        }

        $this->data['restaurant_sidebar'] = $this->load->view("template/restaurant_sidebar", '', true);
        $this->_render('pages/test',$renderData, false);
    }
    public function editTableLayout(){
        $revCenter = ParseCloud::run("editTableLayout",
            array(
                "revId"=>$_GET['revId'],
                "tableLayout"=>$_POST['tableLayout'],
                "chairLayout"=>$_POST['chairLayout'],
            )
        );
    }

}
