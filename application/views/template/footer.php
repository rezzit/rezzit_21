<footer id="footer" class="container">
	<div class="row">
        <div class="col-md-2"><img src="../../resources/img/rezzit21-logo.png" class="img-responsive" /></div>
        <div class="col-md-1"><a href="signup">Sign up</a></div>
        <div class="col-md-1"><a href="aboutus">About Us</a></div>
		<div class="col-md-1"><a href="blog">Blog</a></div>
                <div class="col-md-2"><a href="contactus">Contact Us</a></div>
                <div class="col-md-1"><a href="support">Support</a></div>
                <div class="col-md-2"><a href="termsandconditions">Terms &amp; Conditions</a></div>
                <div class="col-md-2">&copy; 2015 Rezzit<sup>21</sup></div>
	</div>
	
</footer>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-71757377-1', 'auto');
    ga('send', 'pageview');

</script>