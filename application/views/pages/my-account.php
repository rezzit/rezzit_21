<style>
    <style>
    #main{
        background: white;
    }
    h4{
        margin-top: -5px;
    }
    .gridArea{
        border: thin solid #ccc;
        background: #dcdcdc;
        text-align: center;
        margin-top: 20px;
        font-size: 12px;
        padding: 20px;
    }
    .navBit{
        border-top: thin solid #ccc;
        padding: 0 20px;
        padding-top: 20px;
        margin: auto -20px;
        margin-bottom: 20px;
    }
    .gridArea img{
        border-radius: 100px;
        width: 200px;
        height: 200px;
        margin: auto;
        display: block;
    }
    .styled-select select {
        background: transparent;
        font-size: 12px;
        line-height: 1;
        border: 0;
        border-radius: 0;
        -webkit-appearance: none;
        color:black;
    }
    .styled-select {
        padding: 0;
        padding-right: 32px;
        overflow: hidden;
        background: #fff url("http://www.scottgood.com/jsg/blog.nsf/images/arrowdown.gif") no-repeat 98% 50%;
        border: 1px solid #ccc;
        /* background-position: center right; */
        margin: 3px;
        width: 100%;
    }
    input,select{
        width: 100% !important;
        display: block !important;
        padding: 10px;
        margin: 3px;
    }
</style>
<div class="container">
  <?=$diner_sidebar?>
  <div class="col-md-12">
  <form method="post" action="myaccount/editProfile">
   <div class="row gridArea">
      <div class="col-md-6">
         <h3>Personal Information</h3>
         <div class="row">
              <div class="col-md-2">
                  <input name="title" placeholder="Title" value="<?php echo $uData->title;?>">
              </div>
              <div class="col-md-8">
                  <input data-validate="required" name="first_name" placeholder="First Name" value="<?php echo $uData->first_name;?>">
              </div>
              <div class="col-md-2">
                  <input name="middle_initial" placeholder="MI"  value="<?php echo $uData->middle_initial;?>">
              </div>
         </div>
         <div class="row">
              <div class="col-md-9">
                  <input data-validate="required" name="last_name" placeholder="Last Name"  value="<?php echo $uData->last_name;?>">
              </div>
              <div class="col-md-3">
                  <input name="suffix" placeholder="Suffix"  value="<?php echo $uData->suffix;?>">
              </div>
         </div>
         <div class="row">
              <div class="col-md-3">
                  Birthday 
              </div>
              <div class="col-md-3">
                  <select name="birthday_month">
                      <option  value="<?php echo $uData->birthday_month;?>"> - <?php echo ($uData->birthday_month)?$uData->birthday_month:"Month";?> - </option>
                      <option value="January">January</option>
                      <option value="Febuary">Febuary</option>
                      <option value="March">March</option>
                      <option value="April">April</option>
                      <option value="May">May</option>
                      <option value="June">June</option>
                      <option value="July">July</option>
                      <option value="August">August</option>
                      <option value="September">September</option>
                      <option value="October">October</option>
                      <option value="November">November</option>
                      <option value="December">December</option>
                  </select>
              </div>
              <div class="col-md-3">
                  <select name="birthday_day">
                      <option value="<?php echo $uData->birthday_day;?>"> - <?php echo ($uData->birthday_day)?$uData->birthday_day:"Day";?> - </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                  </select>
              </div>
              <div class="col-md-3">
                  <select name="birthday_year">
                      <option value="<?php echo $uData->birthday_year;?>"> - <?php echo ($uData->birthday_year)?$uData->birthday_year:"Year";?> - </option>
                      <option value="2015">2015</option>
                      <option value="2014">2014</option>
                      <option value="2013">2013</option>
                      <option value="2012">2012</option>
                      <option value="2011">2011</option>
                      <option value="2010">2010</option>
                      <option value="2009">2009</option>
                      <option value="2008">2008</option>
                      <option value="2007">2007</option>
                      <option value="2006">2006</option>
                      <option value="2005">2005</option>
                      <option value="2004">2004</option>
                      <option value="2003">2003</option>
                      <option value="2002">2002</option>
                      <option value="2001">2001</option>
                      <option value="2000">2000</option>
                      <option value="1999">1999</option>
                      <option value="1998">1998</option>
                      <option value="1997">1997</option>
                      <option value="1996">1996</option>
                      <option value="1995">1995</option>
                      <option value="1994">1994</option>
                      <option value="1993">1993</option>
                      <option value="1992">1992</option>
                      <option value="1991">1991</option>
                      <option value="1990">1990</option>
                      <option value="1989">1989</option>
                      <option value="1988">1988</option>
                      <option value="1987">1987</option>
                      <option value="1986">1986</option>
                      <option value="1985">1985</option>
                      <option value="1984">1984</option>
                      <option value="1983">1983</option>
                      <option value="1982">1982</option>
                      <option value="1981">1981</option>
                      <option value="1980">1980</option>
                      <option value="1979">1979</option>
                      <option value="1978">1978</option>
                      <option value="1977">1977</option>
                      <option value="1976">1976</option>
                      <option value="1975">1975</option>
                      <option value="1974">1974</option>
                      <option value="1973">1973</option>
                      <option value="1972">1972</option>
                      <option value="1971">1971</option>
                      <option value="1970">1970</option>
                      <option value="1969">1969</option>
                      <option value="1968">1968</option>
                      <option value="1967">1967</option>
                      <option value="1966">1966</option>
                      <option value="1965">1965</option>
                      <option value="1964">1964</option>
                      <option value="1963">1963</option>
                      <option value="1962">1962</option>
                      <option value="1961">1961</option>
                      <option value="1960">1960</option>
                      <option value="1959">1959</option>
                      <option value="1958">1958</option>
                      <option value="1957">1957</option>
                      <option value="1956">1956</option>
                      <option value="1955">1955</option>
                      <option value="1954">1954</option>
                      <option value="1953">1953</option>
                      <option value="1952">1952</option>
                      <option value="1951">1951</option>
                      <option value="1950">1950</option>
                      <option value="1949">1949</option>
                      <option value="1948">1948</option>
                      <option value="1947">1947</option>
<option value="1946">1946</option>
<option value="1945">1945</option>
<option value="1944">1944</option>
<option value="1943">1943</option>
<option value="1942">1942</option>
<option value="1941">1941</option>
<option value="1940">1940</option>
<option value="1939">1939</option>
<option value="1938">1938</option>
<option value="1937">1937</option>
<option value="1936">1936</option>
<option value="1935">1935</option>
<option value="1934">1934</option>
<option value="1933">1933</option>
<option value="1932">1932</option>
<option value="1931">1931</option>
<option value="1930">1930</option>
<option value="1929">1929</option>
<option value="1928">1928</option>
<option value="1927">1927</option>
<option value="1926">1926</option>
<option value="1925">1925</option>
<option value="1924">1924</option>
<option value="1923">1923</option>
<option value="1922">1922</option>
<option value="1921">1921</option>
<option value="1920">1920</option>
<option value="1919">1919</option>
<option value="1918">1918</option>
<option value="1917">1917</option>
<option value="1916">1916</option>
<option value="1915">1915</option>
<option value="1914">1914</option>
<option value="1913">1913</option>
<option value="1912">1912</option>
<option value="1911">1911</option>
<option value="1910">1910</option>
<option value="1909">1909</option>
<option value="1908">1908</option>
<option value="1907">1907</option>
<option value="1906">1906</option>
<option value="1905">1905</option>
<option value="1904">1904</option>
<option value="1903">1903</option>
<option value="1902">1902</option>
<option value="1901">1901</option>
<option value="1900">1900</option>
                  </select>
              </div>
         </div>
          <div class="row">
              <div class="col-md-3">
                  Anniversary 
              </div>
              <div class="col-md-3">
                  <select name="anniversary_month">
                      <option value="<?php echo $uData->anniversary_month;?>"> - <?php echo ($uData->anniversary_month)?$uData->anniversary_month:"Month";?> - </option>
                      <option value="January">January</option>
                      <option value="Febuary">Febuary</option>
                      <option value="March">March</option>
                      <option value="April">April</option>
                      <option value="May">May</option>
                      <option value="June">June</option>
                      <option value="July">July</option>
                      <option value="August">August</option>
                      <option value="September">September</option>
                      <option value="October">October</option>
                      <option value="November">November</option>
                      <option value="December">December</option>
                  </select>
              </div>
              <div class="col-md-3">
                  <select name="anniversary_day">
                      <option value="<?php echo $uData->anniversary_day;?>"> - <?php echo ($uData->anniversary_day)?$uData->anniversary_day:"Day";?> - </option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="29">29</option>
                      <option value="30">30</option>
                      <option value="31">31</option>
                  </select>
              </div>
              <div class="col-md-3">
                  <select name="anniversary_year">
                      <option value="<?php echo $uData->anniversary_year;?>"> - <?php echo ($uData->anniversary_year)?$uData->anniversary_year:"Year";?> - </option>
                      <option value="2015">2015</option>
                      <option value="2014">2014</option>
                      <option value="2013">2013</option>
                      <option value="2012">2012</option>
                      <option value="2011">2011</option>
                      <option value="2010">2010</option>
                      <option value="2009">2009</option>
                      <option value="2008">2008</option>
                      <option value="2007">2007</option>
                      <option value="2006">2006</option>
                      <option value="2005">2005</option>
                      <option value="2004">2004</option>
                      <option value="2003">2003</option>
                      <option value="2002">2002</option>
                      <option value="2001">2001</option>
                      <option value="2000">2000</option>
                      <option value="1999">1999</option>
                      <option value="1998">1998</option>
                      <option value="1997">1997</option>
                      <option value="1996">1996</option>
                      <option value="1995">1995</option>
                      <option value="1994">1994</option>
                      <option value="1993">1993</option>
                      <option value="1992">1992</option>
                      <option value="1991">1991</option>
                      <option value="1990">1990</option>
                      <option value="1989">1989</option>
                      <option value="1988">1988</option>
                      <option value="1987">1987</option>
                      <option value="1986">1986</option>
                      <option value="1985">1985</option>
                      <option value="1984">1984</option>
                      <option value="1983">1983</option>
                      <option value="1982">1982</option>
                      <option value="1981">1981</option>
                      <option value="1980">1980</option>
                      <option value="1979">1979</option>
                      <option value="1978">1978</option>
                      <option value="1977">1977</option>
                      <option value="1976">1976</option>
                      <option value="1975">1975</option>
                      <option value="1974">1974</option>
                      <option value="1973">1973</option>
                      <option value="1972">1972</option>
                      <option value="1971">1971</option>
                      <option value="1970">1970</option>
                      <option value="1969">1969</option>
                      <option value="1968">1968</option>
                      <option value="1967">1967</option>
                      <option value="1966">1966</option>
                      <option value="1965">1965</option>
                      <option value="1964">1964</option>
                      <option value="1963">1963</option>
                      <option value="1962">1962</option>
                      <option value="1961">1961</option>
                      <option value="1960">1960</option>
                      <option value="1959">1959</option>
                      <option value="1958">1958</option>
                      <option value="1957">1957</option>
                      <option value="1956">1956</option>
                      <option value="1955">1955</option>
                      <option value="1954">1954</option>
                      <option value="1953">1953</option>
                      <option value="1952">1952</option>
                      <option value="1951">1951</option>
                      <option value="1950">1950</option>
                      <option value="1949">1949</option>
                      <option value="1948">1948</option>
                      <option value="1947">1947</option>
<option value="1946">1946</option>
<option value="1945">1945</option>
<option value="1944">1944</option>
<option value="1943">1943</option>
<option value="1942">1942</option>
<option value="1941">1941</option>
<option value="1940">1940</option>
<option value="1939">1939</option>
<option value="1938">1938</option>
<option value="1937">1937</option>
<option value="1936">1936</option>
<option value="1935">1935</option>
<option value="1934">1934</option>
<option value="1933">1933</option>
<option value="1932">1932</option>
<option value="1931">1931</option>
<option value="1930">1930</option>
<option value="1929">1929</option>
<option value="1928">1928</option>
<option value="1927">1927</option>
<option value="1926">1926</option>
<option value="1925">1925</option>
<option value="1924">1924</option>
<option value="1923">1923</option>
<option value="1922">1922</option>
<option value="1921">1921</option>
<option value="1920">1920</option>
<option value="1919">1919</option>
<option value="1918">1918</option>
<option value="1917">1917</option>
<option value="1916">1916</option>
<option value="1915">1915</option>
<option value="1914">1914</option>
<option value="1913">1913</option>
<option value="1912">1912</option>
<option value="1911">1911</option>
<option value="1910">1910</option>
<option value="1909">1909</option>
<option value="1908">1908</option>
<option value="1907">1907</option>
<option value="1906">1906</option>
<option value="1905">1905</option>
<option value="1904">1904</option>
<option value="1903">1903</option>
<option value="1902">1902</option>
<option value="1901">1901</option>
<option value="1900">1900</option>
                  </select>
              </div>
         </div>
          <div class="row">
              <div class="col-md-6">
                  <h3>Allergies</h3>
                  <div class="row">
                      <select name="allergies" multiple>
                          <option val="1">Fish</option>
                          <option val="2">Milk</option>
                          <option val="3">Gluten</option>
                      </select>
                  </div>
              </div>
              <div class="col-md-6">
                  <h3>Set Default Homepage</h3>
                  <div class="row">
                      <select name="default_home_page">
                          <option val="1">Reservations</option>
                          <option val="2">Favorites</option>
                          <option val="3">Profile</option>
                      </select>
                  </div>
              </div>
          </div>

      </div>
      <div class="col-md-6">
         <h3>Contact Information</h3>
         <div class="row">
              <div class="col-md-12">
                  <input data-validate="required" name="address_one" placeholder="Address #1"  value="<?php echo $uData->address_one;?>">
              </div>
         </div>
         <div class="row">
              <div class="col-md-12">
                  <input name="address_two" placeholder="Address #2"  value="<?php echo $uData->address_two;?>">
              </div>
         </div>
         <div class="row">
              <div class="col-md-6">
                 <input data-validate="required" name="city" placeholder="City"  value="<?php echo $uData->city;?>">
              </div>
              <div class="col-md-3">
                 <select name="state" placeholder="State"> <?=$states?> </select>
              </div>
              <div class="col-md-3">
                 <input data-validate="required,number,min(5)" name="zip" placeholder="Zip"  value="<?php echo $uData->zip;?>">
              </div>
         </div> 
         <div class="row">
              <div class="col-md-6">
                 <input data-validate="required,validPhone" class="phone_mask" name="phone_one" placeholder="Phone #1"  value="<?php echo $uData->phone_one;?>">
              </div>
             <div class="col-md-6">
                 <input data-validate="validPhone" class="phone_mask" name="phone_two" placeholder="Phone #2"  value="<?php echo $uData->phone_two;?>">
             </div>
         </div>
         <div class="row">
             <div class="col-md-6">
                 <input data-validate="validPhone" class="phone_mask" name="mobile" placeholder="Mobile"  value="<?php echo $uData->mobile;?>">
             </div>
             <div class="col-md-6">
                 <input data-validate="validPhone" class="phone_mask" name="fax" placeholder="Fax"  value="<?php echo $uData->fax;?>">
             </div>
         </div>
         <h3>Employment Information</h3>
         <div class="row">
              <input name="employment_company" placeholder="Company"  value="<?php echo $uData->employment_company;?>">
         </div>
         <div class="row">
              <input name="employment_position" placeholder="Position"  value="<?php echo $uData->employment_position;?>">
         </div>
         <div class="row">
              <input data-validate="url" name="employment_website" placeholder="Website"  value="<?php echo $uData->employment_website;?>">
         </div>

      </div>
   </div>

  </form>
      <div class="row">
          <button style="width: 100%;background-color: #AEAEF7;border:none;padding: 10px;color: white;" name="save_button" >Save</button>      </div>
  </div>

</div>
