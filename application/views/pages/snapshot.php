<div class="container">
   <?=$restaurant_sidebar?>
   <div class="col-md-6 col-md-pull-3 gridArea">
     <h2>Restaurant management: <?php echo $myRestaurant->name;?></h2>
     <div class="row">
        <div class="col-md-6">
           <div class="row">
                Pre-Order Alerts: 0
           </div>
        </div>
        <div class="col-md-6">
           <div class="row">
                Reservations Made Today: 0
           </div>
        </div>
     </div>
     <div class="row">
        <div class="col-md-6">
           <form action="post">
                <h3>New Notification</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <input data-validate="required" class="the_date" name="due_date" placeholder="Due Date">
                        </div>
                        <div class="col-md-6">
                            <input name="time" placeholder="Time">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input data-validate="required" name="title" placeholder="Title">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input name="content" placeholder="Content">
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-md-12">
                            <button type="submit">Add</button>
                        </div>
                    </div>
           </form>   
        </div>
    </div>
   </div>
</div>
