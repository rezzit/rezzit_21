<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH.'third_party/parse/autoload.php');


use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseACL;
use Parse\ParsePush;
use Parse\ParseUser;
use Parse\ParseInstallation;
use Parse\ParseException;
use Parse\ParseAnalytics;
use Parse\ParseFile;
use Parse\ParseCloud;
use Parse\ParseClient;

ParseClient::initialize('0u1LGaR91Mu6HPFtsP3SnUrGZ32NftzJF6hrkSad', '5MfN2qUvyYNFjOgqCtu8mUvV2R7VHk8rzoREfTuM', 'cnb8TPDLSKCAQ3MM7KI9BA8O5cculC4DQuHChTNP');

class revCenter extends MY_Controller
{

    public function index($renderData = "")
    {
if($_GET['action'] == "edit"){

}else{
    header("location:../../revCenter/addRevCenter?id=".$_GET['id']);
}
    }
    public function addRevCenter($renderData = ""){
        $this->title = "Rezzit21";
        $this->keywords = "arny, arnodo";
        $this->data['revProfile'] = null;
        $this->data['restaurant_sidebar'] = $this->load->view("template/restaurant_sidebar", '', true);
        $this->_render('pages/revenue-profile',$renderData, false);
    }
    public function editRevCenter($renderData = ""){
        $this->title = "Rezzit21";
        $this->keywords = "arny, arnodo";

        $list = ParseCloud::run("GetRevCenter",array("restaurant"=>$_GET['id']));
        foreach($list as $revCenter){
            if($revCenter->getObjectId() == $_GET['revId']){
                $this->data['revProfile'] = $revCenter;
            }
        }

        $this->data['restaurant_sidebar'] = $this->load->view("template/restaurant_sidebar", '', true);
        $this->_render('pages/revenue-profile',$renderData, false);
    }
    public function processRevData(){

        $account = ParseCloud::run("GetUserByEmail",array("contact"=>$_SESSION['username']));

        $myRestaurants = ParseCloud::run("GetRestaurant",
            array(
                "userid"=>$account[0]->getObjectId(),
                "restaurant"=>$_GET['id']
            )
        );

        if(!$_GET['revId']){
            $revCenter = ParseCloud::run("createRevCenter",
                array(
                    "restaurant"=>$myRestaurants[0]->getObjectId(),
                    "price"=>$_POST['price'],
                    "visible"=>$_POST['visible'],
                    "cuisine"=>$_POST['cuisine'],
                    "display_name"=>$_POST['display_name'],
                    "benefits"=>$_POST['benefits'],
                    "dressCode"=>$_POST['dressCode'],
                    "facebook_url"=>$_POST['facebook_url'],
                    "twitter_url"=>$_POST['twitter_url'],
                    "instagram_url"=>$_POST['instagram_url'],
                    "youtube_url"=>$_POST['youtube_url'],
                    "pinterest_url"=>$_POST['pinterest_url'],
                    "hourJSON"=>$_POST['hourJSON'],
                    "openJSON"=>$_POST['openTimes']
                )
            );
        }else{
            $start = strtotime($_POST['to']);
            $end = strtotime($_POST['from']);
            $range = array();
            while ($start !== $end)
            {
                $range[] = date('h:ia', $start);
                $start = strtotime('+30 minutes',$start);
            }
            print_r($range);

            $revCenter = ParseCloud::run("editRevCenter",
                array(
                    "revId"=>$_GET['revId'],
                    "price"=>$_POST['price'],
                    "cuisine"=>$_POST['cuisine'],
                    "display_name"=>$_POST['display_name'],
                    "benefits"=>$_POST['benefits'],
                    "dressCode"=>$_POST['dressCode'],
                    "facebook_url"=>$_POST['facebook_url'],
                    "twitter_url"=>$_POST['twitter_url'],
                    "instagram_url"=>$_POST['instagram_url'],
                    "youtube_url"=>$_POST['youtube_url'],
                    "transport"=>$_POST['transport'],
                    "hours"=>$range,
                    "description"=>$_POST['description'],
                    "pinterest_url"=>$_POST['pinterest_url'],
                    "hourJSON"=>$_POST['hourJSON'],
                    "openJSON"=>$_POST['openTimes']
                )
            );
            header("location:editRevCenter?id=".$_GET['id']."&revId=".$_GET['revId']);
        }


    }
}