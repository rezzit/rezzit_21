<?php define("GOOGLE_API_KEY_BROWSER", "AIzaSyAEaFJ30HLY-Hg74IWW2btpMLHHyZ1dev4");?>
<link rel="stylesheet" href="resources/css/animate.css">
<style>
    .loader{
        background-color: #5ec1d8;
        position: fixed;
        z-index: 100000000000000 !important;
        left: 0;
        top: 0;
        padding: 18% 0;
        width: 100%;
        text-align: center;
    }
    .loader div{
        width: 800px;
        margin: auto;
        text-align: center;
        display: block;
        color: #eee;
        font-weight: bold;
    }
    .loader img{
        margin: auto;
    }
    #main{
        background: white;
    }
    .item{
        height: 400px;
    }
    .item .col-md-6{
        text-align: left;
    }
    .carousel-caption{
        right: 10%;
        left: 10%;
        padding-bottom: 30px;
        font-size: 18px;
    }
    .item h1{
        font-size: 40px;
    }
    #img-1{
        background:url(http://52.91.115.160/resources/img/perrys-rotator-pe-20140415-1600x500.png);
        background-size:cover;
        background-position: top left;
    }
    #img-2{
        background:url(http://52.91.115.160/resources/img/secondImg.png);
        background-size:cover;
        background-position: top left;
    }
    #img-3{
        background:url(http://52.91.115.160/resources/img/thirdHero.png);
        background-size:cover;
        background-position: top left;
    }
    .glyphicon{
        color: #fff;
    }
    .fav{
        position: absolute;
        bottom: 30px;
        right: 0;
        text-align: right !important;
    }
    h3{
        border-bottom: thin solid #000;
        padding-bottom: 10px;
        margin-bottom: 10px;
    }
    .copyBlock{
        padding:40px;
    }
    .boxxy{
        border: thin solid #ccc;
        padding: 5px;
        text-align: center;
        margin: 3px;
        width: Calc(25% - 6px);

        background-color: #5ec1d8;
        color: white;
    }
    .stickyAction{
        position: fixed;
        top: 50px;
        right: 50px;
        width: Calc(50% - 50px);
        margin: 0 !important;
        z-index: 10000 !important;
        /*background: #bbb;*/
        height: Calc(100% - 50px);

    }

    #actionArea{
        padding:20px;
        padding-left: 40px;

        font-size:12px;
        text-align: center;
    }
    .stickyAction .stickyBox{
        width: 90%;
        margin: auto;
    }
    .footerArea{
        border-top:thick solid #ccc;
    }
    .footerArea div{
        padding-top: 30px;
    }
    .footerArea div, .footerArea img{
        text-align: center;
        margin: auto;
    }
    footer{
        display: none;
    }
    .menuList{
        border-bottom:thick solid #000;
        margin-bottom: 20px;
    }
    #menu .title{
        border-bottom:thick solid #000;
        margin-bottom: 20px;

        border-top:thick solid #000;
        margin-top: 20px;
    }

</style>
<script>
    $(document).ready(function(){
//        $.ajax({
//            type: "POST",
//            url: "restaurantdetails/findMe",
//            data: {
//            },
//
//            success: function(data, textStatus, jqXHR)
//            {
//                var i = 0;
//                var array = JSON.parse(data);
//                console.log(array);
//                array = array['_embedded']['categories'];
//                array.forEach(function(object) {
//                    $("#menu").append("<div class='row' id='cat-"+i+"'><div class='col-md-12 title'>"+object.name+"</div></div>");
//                    object._embedded.items.forEach(function(object){
//                        $("#cat-"+i).append("<div class='col-md-12'>"+object.name+"</div>");
//                    });
////                    $("#menu").find("#menu"+object.modifier_groups_count).append("<div class='col-md-6'><div class='row'><div class='col-md-6'>"+object.name+"</div><div class='col-md-6'>"+object.price+"</div></div></div>");
//////                    $(".revCenter[data-id='object.objectId']").find()
//////                    if($(ref).attr('data-max') < object.avail_seats){
//////                        $(ref).attr('data-max', object.avail_seats);
//////                    }
//                    i++;
//                });
////                if($(ref).attr('data-max') == 0){
////                    $(ref).addClass('overlay').parents("a").attr("href","");
////                }
////                $("#menu").html(data);
//            }
//        });
        $(".boxxy").each(function(){
            var ref = $(this);
            $.ajax({
                type: "POST",
                url: "searchrestaurant/getAvailSeats?id=<?=$_GET['id']?>&time="+$(ref).text(),
                data: {
                },

                success: function(data, textStatus, jqXHR)
                {
                    if(data){
                        var array = JSON.parse(data);
                        array.forEach(function(object) {
                            if($(ref).attr('data-max') < object.avail_seats && $(ref).parents(".revCenter").attr("data-id") == object.objectId){
                                $(ref).attr('data-max', object.avail_seats);
                            }
                        });
                        if($(ref).attr('data-max') == 0){
                            $(ref).addClass('overlay').parents("a").attr("href","").hide();
                        }
                        $(".loader").hide();
                    }
                }
            });
        });

        $('.hideMe').parents('.revCenter').hide();
        var stickyNavTop = $('#actionArea').offset().top;

        var stickyAction = function(){
            var scrollTop = $(window).scrollTop();

            if ((scrollTop) > stickyNavTop) {
                $('#actionArea').addClass('stickyAction');
            } else {
                $('#actionArea').removeClass('stickyAction');
            }

        };

        stickyAction();

        $(window).scroll(function() {
            stickyAction();
        });
        });

</script>
<div class="loader">
    <div>
        <img src="http://rezzit21.com/resources/img/Rezzite%20Logo%20Large%20White.png" class="img-responsive animated infinite pulse">
        <p>
            Almost there! Finding available reservations.
        </p>
    </div>
</div>

<div class="container">
   <div class="row">
       <div class="col-md-12">
           <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

               <!-- Wrapper for slides -->
               <div class="carousel-inner" role="listbox">
                   <div class="item active" id="img-1">
                       <div class="carousel-caption">
                           <div class="row">
                               <div class="col-md-6">
                                   <h1><?=$revProfile->name;?></h1>
                                   <div>Anthony Rojas | $$$</div>
                                   <small>Anthony Rojas | $$$</small>
                               </div>
                               <div class="col-md-6 fav">
                                   add to favorites <span class="glyphicon glyphicon-heart"></span>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="item" id="img-2">
                       <div class="carousel-caption">
                           <div class="row">
                               <div class="col-md-6">
                                   <h1><?=$revProfile->name;?></h1>
                                   <div>Anthony Rojas | $$$</div>
                                   <small>Anthony Rojas | $$$</small>
                               </div>
                               <div class="col-md-6 fav">
                                   add to favorites <span class="glyphicon glyphicon-heart"></span>
                               </div>
                           </div>
                       </div>
                   </div>
                   <div class="item" id="img-3">
                       <div class="carousel-caption">
                           <div class="row">
                               <div class="col-md-6">
                                   <h1><?=$revProfile->name;?></h1>
                                   <div>Anthony Rojas | $$$</div>
                                   <small>Anthony Rojas | $$$</small>
                               </div>
                               <div class="col-md-6 fav">
                                   add to favorites <span class="glyphicon glyphicon-heart"></span>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>

               <!-- Controls -->
               <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                   <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                   <span class="sr-only">Previous</span>
               </a>
               <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                   <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                   <span class="sr-only">Next</span>
               </a>
           </div>
       </div>
   </div>

   <div class="row">
       <div class="col-md-6">
           <div class="copyBlock">
               <h3>About <?php echo $revProfile->name;?></h3>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum



           </div>
           <iframe frameborder="0" style="border:0;width:100%;height:350px;" src="https://www.google.com/maps/embed/v1/place?q=<?php echo $revProfile->address_1.", ".$revProfile->city.", ".$revProfile->state;?>&key=<?= GOOGLE_API_KEY_BROWSER ?>"></iframe>
           <div id="menu"></div>
           <?php
                $_SESSION['findMe'] = $revProfile->appId;
           ?>
               <div class="row footerArea">

                   <div class="col-md-3"><a href="signup">Sign up</a></div>
                   <div class="col-md-3"><a href="aboutus">About Us</a></div>
                   <div class="col-md-3"><a href="blog">Blog</a></div>
                   <div class="col-md-3"><a href="contactus">Contact Us</a></div>
                   <div class="col-md-6"><a href="support">Support</a></div>
                   <div class="col-md-6"><a href="termsandconditions">Terms &amp; Conditions</a></div>
                   <div class="col-md-12"><img src="resources/img/rezzit21-logo.png" class="img-responsive"></div>
                   <div class="col-md-12">© 2015 Rezzit<sup>21</sup></div>
               </div>


       </div>
       <div class="col-md-6" id="actionArea">
           <div class="stickyBox">
               <h4>Reserve your table today!</h4>
               <?php
               date_default_timezone_set('US/Eastern');
               $strTime = strtotime($_SESSION['time']);
               foreach($revCenter as $rev) {
                   echo '<div class="row revCenter" data-id="'.$rev->getObjectId().'">';
                   echo '<div class="col-md-12">';
                   echo '<div class="row">';
                   echo '<div class="col-md-12">';
                   echo 'Showing availability for ';
                   echo $rev->display_name;
                   echo ' on ';
                   echo $_SESSION['date'];
                   echo ' after ';
                   echo date("g:ia", strtotime("now"));
                   echo '</div>';
                   echo '</div>';
                   echo '<div class="row">';
                   echo '<div class="col-md-12">';
                   $i = 0;

                   $avgRez = "60";
                   switch($_SESSION['maxSeats']){
                       case '3': case '4':
                       $avgRez = "90";
                       break;
                       case '5': case '6':
                       $avgRez = "120";
                       break;
                       case '7': case '8': case '9':
                       $avgRez = "120";
                       break;
                       case '10':
                           $avgRez = "135";
                           break;
                   }

                   $flag = 0;


                   $rezStart = strtotime($_SESSION['time']);
                   $rezEnd = strtotime($_SESSION['time']." + ".$avgRez." minutes");

                   $jsonHour = json_decode($rev->hourJSON);
//                   var_dump($jsonHour);
                   foreach($jsonHour as $hour){
                       $rezStart = strtotime($hour->to);
                       $rezEnd = strtotime($hour->from);
                       while ($rezStart < $rezEnd) {
                           $rezStart = strtotime('+ 15 minutes',$rezStart);
                           if($rezStart > $strTime){

                               $out = "";
                               $out .=  '<a href="';
                               $out .=  'reservation?id=' . $_GET['id'] . "&revId=" . $rev->getObjectId() . "&time=" . date("g:ia", $rezStart);
                               $out .=  '">';
                               $out .=  '<div data-max="0" data-hour="' . $hour . '" class="col-md-3 boxxy">';
                               $out .=  date("h:ia", $rezStart);
                               $out .=  '</div>';
                               $out .=  '</a>';

                               echo $out;
                           }
                           $i++;
                       }
                   }




                   if ($i == 0) {
                       echo '<div class="col-md-12">';
                       echo "No reservation times available";
                       echo '<div class="hideMe"></div>';
                       echo '</div>';
                   }
                   echo '</div>';
                   echo '<hr />';
                   echo '</div>';

                   echo '</div>';
                   echo '</div>';

               }
               ?>
           </div>

       </div>
   </div
 </div>

