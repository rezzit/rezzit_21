<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class logout extends MY_Controller {

    public function index($renderData=""){
        session_unset();
        header("location:../");
    }

}
