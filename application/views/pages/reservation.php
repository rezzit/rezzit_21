<?php
//foreach($reservations as $reservation){
//  foreach(json_decode($reservation->seats) as $seat){
//    echo $sear
//  }
//}

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Draggable - Snap to element or grid</title>

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="/resources/js/rotate.js"></script>
  <style>
    .draggable { width: 60px; height: 60px; padding: 5px; display:inline-block; }
    .ui-widget-header p, .ui-widget-content p { margin: 0; }
    #snaptarget { height: 140px; }
    .table, .table-original div{
      background: url(resources/img/Table1.jpg);
      width: 60px; height: 60px; padding: 5px; display:inline-block;
    }
    .chair,.chair-original div{
      background: #000;

      background-size:cover;
      width: 30px;
      height:30px;
      background-position: -6px -1px;
      border-radius: 200px;
    }
    .chair-original{
      padding: 24px;
    }
    #ItemInventory{
      width: 100%;
      overflow-x: scroll;
    }
    .innerScroll{
      width: 300%;
    }
    .bg-area{
      width: 100%;
      height: 500px;
      background: url(http://dinetimehost.com/img/features/Features-Floor1.jpg);
      background-size: cover;
      background-position: 0 0;
    }

    .show {
      z-index:1000;
      position: absolute;
      background-color:#C0C0C0;
      border: 1px solid blue;
      padding: 2px;
      display: block;
      margin: 0;
      list-style-type: none;
      list-style: none;
    }

    .hide {
      display: none;
    }

    #rmenu ul{
      padding:10px;
      margin: 0px;
    }
    /*.seat{*/
    /*background: red;*/
    /*width: 20px;*/
    /*height: 20px;*/
    /*}*/
    .top{
      position:absolute;
      left: 20px;
      top:0;
    }
    .left{
      position: absolute;
      top: 20px;
      left: 0px;
    }
    .right{
      position: absolute;
      top: 20px;
      left: 40px;
    }
    .bottom{
      position: absolute;
      top: 40px;
      left: 20px;
    }
    .tNum{
      display: inline-block;
      background-color: white;
      border-radius: 1000px;
      width: 30px;
      height: 30px;
      font-size: 20px;
      position: absolute;
      text-align: center;
      top: 15px;
      left: 15px;
    }
    .show li{ list-style: none; }
    .show a { border: 0 !important; text-decoration: none; }
    .show a:hover { text-decoration: underline !important; }
    #success, #Failure{
      width: 100%;
      height: 100%;

      padding: 25% 30%;
      font-size: 30px;
      color: white;
      background:rgba(0,0,0,0.9);
      position: fixed;
      top: 0;
      left: 0;
      display: none;
      text-align: center;
    }
    button div{
      margin-bottom: 0px !important;
      margin-left: 0px !important;
      margin-top: 5px !important;
    }
    h3{
      text-align: center;
      padding-bottom: 10px;
    }
    #submitLayout{
      width: 100%;
      /* height: 200px; */
      display: block;
      text-align: center;
      padding: 25px;
      background-color: #AEAEF7;
      color: white;
      font-weight: bold;
      margin-bottom: -50px;
    }
    .chair:hover, .table:hover{
      background: white;
      border: thick solid black;
    }
footer{
  display: none;}
    #main{
      margin:0;
      padding: 0;
    }
    .actionBar{
      position: fixed;
      top:0;
      right: 0;
      width: 20%;
      height:100%;
      background:#ddd;
      border:thin solid #ccc;
      margin-top:50px;
    }
    .actionLabel{
      padding: 12px;
      text-align: center;
      font-size: 18px;
      border-bottom: thin solid #ccc;
    }
    .draggable { width: 60px; height: 60px; padding: 5px; display:inline-block; }
    .ui-widget-header p, .ui-widget-content p { margin: 0; }
    #snaptarget { height: 140px; }
    .innerScroll button{
      padding: 10px;
      height:150px;
      width: 150px;
    }
    .innerScroll button div{
      display:block;
      font-size: 12px;
      font-weight: bold;
      position: relative;
      top:55px;
    }
    .table{
      width: 80px;
      height: 80px;
      padding: 5px;
      display: inline-block;
    }

    .table-original{
      background: url(resources/img/tbl/square.png);
      background-position: 0 -10px;
      background-size: 100%;
      background-repeat: no-repeat;
    }
    .table-square{
      background: url(resources/img/tbl/square.png);
      background-position: center center;
      /*background-size: 145% 140%;*/
      background-size: 150%;
      background-repeat: no-repeat;
    }


    .table-sqBooth-original{
      background: url(resources/img/tbl/Square-Booth.png);

      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-sqBooth{
      background: url(resources/img/tbl/Square-Booth.png);

      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-rectangle-original{
      background: url(resources/img/tbl/Rectangle.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-rectangle{
      height: 55px;
      background: url(resources/img/tbl/Rectangle.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-rBooth-original{
      background: url(resources/img/tbl/Rectangle-booth.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-rBooth{
      height: 55px;
      background: url(resources/img/tbl/Rectangle-booth.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-cTable-original{
      background: url(resources/img/tbl/Circle.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-cTable{
      background: url(resources/img/tbl/Circle.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-cBooth-original{
      background: url(resources/img/tbl/Circle-Booth.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-cBooth{
      background: url(resources/img/tbl/Circle-Booth.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .ui-widget-content{
      border:none;
    }
    .chair,.chair-original{
      background: #444;

      background-size:cover;
      width: 30px;
      height:30px;
      background-position: -6px -1px;
      border-radius: 200px;
    }
    .chair-original{
      padding: 24px;
      position: relative;
      margin: auto;
      top: 10px;
    }
    #ItemInventory{
      width: 100%;
      overflow-x: scroll;
    }
    .innerScroll{
      width: 300%;
    }
    .bg-area{
      width: 80%;
      height: 800px;

      background: url(http://dinetimehost.com/img/features/Features-Floor1.jpg);
      background-size: 100% 100%;

      background-position: 0px 43px;
      background-repeat: no-repeat;
    }

    .show {
      z-index:1000;
      position: absolute;
      background-color:#C0C0C0;
      border: 1px solid blue;
      padding: 2px;
      display: block;
      margin: 0;
      list-style-type: none;
      list-style: none;
    }

    .hide {
      display: none;
    }

    #rmenu ul{
      padding:10px;
      margin: 0px;
    }
    /*.seat{*/
    /*background: red;*/
    /*width: 20px;*/
    /*height: 20px;*/
    /*}*/
    .top{
      position:absolute;
      left: 20px;
      top:0;
    }
    .left{
      position: absolute;
      top: 20px;
      left: 0px;
    }
    .right{
      position: absolute;
      top: 20px;
      left: 40px;
    }
    .bottom{
      position: absolute;
      top: 40px;
      left: 20px;
    }
    .tNum{
      display: inline-block;
      background-color: white;
      border-radius: 1000px;
      width: 30px;
      height: 30px;
      font-size: 20px;
      position: absolute;
      text-align: center;
      top: 15px;
      left: 25px;
    }
    .show li{ list-style: none; }
    .show a { border: 0 !important; text-decoration: none; }
    .show a:hover { text-decoration: underline !important; }
    #success, #Failure{
      width: 100%;
      height: 100%;

      padding: 25% 30%;
      font-size: 30px;
      color: white;
      background:rgba(0,0,0,0.9);
      position: fixed;
      top: 0;
      left: 0;
      display: none;
      text-align: center;
    }
    button div{
      margin-bottom: 0px !important;
      margin-left: 0px !important;
      margin-top: 5px !important;
    }
    h3{
      text-align: center;
      border-bottom: thin solid #DADADA;
      padding-bottom: 10px;
    }
    #rotateTable{
      width: 20%;
      display: block;
      text-align: center;
      padding: 15px;
      background-color: #bbb;
      color: white;
      font-weight: bold;
      position: fixed;
      bottom: 50px;
      right: 0;
    }

    #chairButton{
      top:25px;
    }
  </style>
  <script>
    var i = 0;
    var tCount = 1;
    $(function() {
      $( ".table" ).draggable({ snap: true });

      $(".chair-original").click(function(){
        $(".bg-area").append('<div data-eleType="1" id="ele-'+i+'" class="chair draggable ui-widget-content"></div>');

        i++;
      });
      $(".table-original").click(function(){
        $(".bg-area").append('<div data-premium="0" data-eleType="2" id="ele-'+i+'"  class="table draggable ui-widget-content"><p class="top seat"></p><p class="left seat"></p><p class="right seat"></p><p class="bottom seat"></p><p class="tNum">'+tCount+'</p></div>');
        tCount++;
        i++;


      });

    });

    $(document).ready(function() {
      var tableLayout = '<?=$tableLayout?>';
      var chairLayout = '<?=$chairLayout?>';

      $('#submitSignup').click(function(){
        $.ajax({
          type: "POST",
          url: "reservation/payReservation",
          data: {
            firstName: $('#myModal input[name="firstName"]').val(),
            lastName: $('#myModal input[name="lastName"]').val(),
            email: $('#myModal input[name="email"]').val(),
            phone: $('#myModal input[name="phone"]').val()
          },

          success: function(data, textStatus, jqXHR)
          {
            window.location.replace("reservationthankyou");
          }
        });
      });


      if(tableLayout){
        var array = JSON.parse(tableLayout);
        array.forEach(function(object) {
          $(".bg-area").append('<div data-eleType="'+object.eleType+'" id="ele-'+i+'" class="'+object.eleType+' table draggable ui-widget-content"><p class="top seat"></p><p class="left seat"></p><p class="right seat"></p><p class="bottom seat"></p><p class="tNum">'+tCount+'</p></div>');

          $( ".table" ).draggable({ snap: ".table", snapMode: "outer",
            stop: function (event, ui) {
              /* Get the possible snap targets: */
              var snapped = $(this).data('ui-draggable').snapElements;

              /* Pull out only the snap targets that are "snapping": */
              var snappedTo = $.map(snapped, function (element) {
                return element.snapping ? element : null;
              });
              if(snappedTo[0]){
                $('#'+snappedTo[0]['item']['id']);
              }
            }
          });
          $('#ele-'+i).css({
            'position':'absolute',
            'top':object.yPos+'px',
            'left':object.xPos+'px',
            '-webkit-transform' : object.rotate,
            '-moz-transform' : object.rotate,
            '-ms-transform' : object.rotate,
            '-o-transform' : object.rotate,
            'transform' : object.rotate,
            'zoom' : 1
          });
          console.log(object.rotate);

          tCount++;
          i++;
        });
      }

      if(chairLayout){

        var chairArray = JSON.parse(chairLayout);

        chairArray.forEach(function(object) {
          $(".bg-area").append('<div data-eleType="1" id="ele-'+i+'" class="chair draggable ui-widget-content"></div>');

          $('#ele-'+i).css({
            'position':'absolute',
            'top':object.yPos+'px',
            'left':object.xPos+'px'
          });
          i++;
        });
        var maxSeats = <?=$_SESSION['maxSeats'];?>;
        $(".chair").click(function(){
          if($(this).attr('data-selection') == "selected"){
            $(this).removeAttr('data-selection').css("background","black");
          }else{
            if($(".table[data-selection='selected']").length >= 2 || $(".chair[data-selection='selected']").length >= maxSeats){
              alert("You've chosen too many seats. Please deselect a seat before continuing.");
            }else{
              $(this).css("background","white").attr("data-selection","selected");
            }
          }
        });
        var maxSeats = <?=$_SESSION['maxSeats'];?>;
        $(".table").click(function(){
          if($(this).attr('data-selection') == "selected"){
            $(this).removeAttr('data-selection').css("border","none");
          }else{
            if($(".table[data-selection='selected']").length >= 2 || $(".chair[data-selection='selected']").length >= maxSeats){
              alert("You've chosen too many seats. Please deselect a seat before continuing.");
            }else{
              $(this).css("border","thick solid black").attr("data-selection","selected");
            }
          }
        });

      }


      $('#submitLayout').click(function(){
        var tables = [];
        var chairs = [];

        $('.table').each(function(){
          var offset = 0;
          if(
              $(this).css('transform') == "matrix(0.707107, 0.707107, -0.707107, 0.707107, 0, 0)" ||
              $(this).css('transform') == 'matrix(-0.707107, 0.707107, -0.707107, -0.707107, 0, 0)' ||
              $(this).css('transform') == "matrix(0.707107, -0.707107, 0.707107, 0.707107, 0, 0)" ||
              $(this).css('transform') == 'matrix(-0.707107, -0.707107, 0.707107, -0.707107, 0, 0)'
          ){
            offset = 10;
          }
          tables.push({
            xPos: $(this).offset().left + offset,
            yPos: $(this).offset().top + offset,
            rotate: $(this).css('transform'),
            premium: $(this).attr('data-premium'),
            eleType: $(this).attr('data-eleType')
          });
        });
        $(".chair[data-selection='selected']").each(function(){
          chairs.push({
            "item": "Chair",
            "id": $(this).attr('id')
          });
        });
        $(".table[data-selection='selected']").each(function(){
          chairs.push({
            "item": "Table",
            "id": $(this).attr('id')
          });
        });

        $.ajax({
          type: "POST",
          url: "/reservation/setReservation?&restId=<?=$_GET['id']?>&time=<?=$_GET['time']?>&revId=<?=$_GET['revId']?>",
          data: {
            seats: JSON.stringify(chairs)
          },

          success: function(data, textStatus, jqXHR)
          {
            console.log("Buffer Start()");
            console.log(data);
            console.log("Buffer End()");
            $('#myModal').modal('show');
          }
        });
      });

      $('.rFurn').click(function(){
        $('#'+$('.rFurn').attr('data-remove')).remove();
      });
      $('.editPremium').click(function(){
        if($('#'+$('.rFurn').attr('data-remove')).attr(
                "data-premium"
            ) == 1){
          $('#'+$('.rFurn').attr('data-remove')).attr(
              "data-premium","0"
          ).find('.tNum').css({
            'background':'white',
            'color':'black'
          });
        }else{
          $('#'+$('.rFurn').attr('data-remove')).attr(
              "data-premium","1"
          ).find('.tNum').css({
            'background':'red',
            'color':'white'
          });
        }

      });



    });




  </script>
</head>
<body>

<div id="ItemInventory">
  <h3>Seating selection</h3>
</div>
<hr />
<div class="top_bar" style="text-align:center">
  <button class="button green">Select Table</button>
  <button class="button unselected green">Pre-Order/Payment</button>
  <button class="button unselected green">Confirmation</button>
</div>
<br style="clear:both">

<div  class="bg-area">

</div>

<div id="success">Successfully saved table layout!</div>
<div id="Failure">Failed saving table layout</div>

<!-- initially hidden right-click menu -->
<div class="hide" id="rmenu">
  <ul>
    <li>
      <div class="editPremium">Toggle Premium</div>
    </li>
    <li>
      <div class="rFurn">Remove Furniture</div>
    </li>
  </ul>
</div>
<div id="submitLayout">Select seat</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Reservation Details</h4>
      </div>
      <form>
        <div class="modal-body">
          <div class="form-group">
            <label for="firstName">First name</label>
            <input type="text" class="form-control" id="firstName" placeholder="John" name="firstName">
          </div>
          <div class="form-group">
            <label for="lastName">Last name</label>
            <input type="text" class="form-control" id="lastName" placeholder="Smith" name="lastName">
          </div>
          <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" placeholder="JohnSmith@msn.com" name="email">
          </div>
          <div class="form-group">
            <label for="phone">Phone</label>
            <input type="text" class="form-control" id="phone" placeholder="8601234567" name="phone">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="submitSignup">Save changes</button>
        </div>
      </form>
    </div>
  </div>
</div>

</body>
</html>