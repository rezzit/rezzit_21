<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>jQuery UI Draggable - Snap to element or grid</title>

  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script type="text/javascript" src="/resources/js/rotate.js"></script>
  <style>
    #main{
      margin:0;
      padding: 0;
    }
    .actionBar{
      position: fixed;
      top:0;
      right: 0;
      width: 20%;
      height:100%;
      background:#ddd;
      border:thin solid #ccc;
      margin-top:50px;
    }
    .actionLabel{
      padding: 12px;
      text-align: center;
      font-size: 18px;
      border-bottom: thin solid #ccc;
    }
    .draggable { width: 60px; height: 60px; padding: 5px; display:inline-block; }
    .ui-widget-header p, .ui-widget-content p { margin: 0; }
    #snaptarget { height: 140px; }
    .innerScroll button{
      padding: 10px;
      height:150px;
      width: 150px;
    }
    .innerScroll button div{
      display:block;
      font-size: 12px;
      font-weight: bold;
      position: relative;
      top:55px;
    }
    .table{
      width: 80px;
      height: 80px;
      padding: 5px;
      display: inline-block;
    }

    .table-original{
      background: url(resources/img/tbl/square.png);
      background-position: 0 -10px;
      background-size: 100%;
      background-repeat: no-repeat;
    }
    .table-square{
      background: url(resources/img/tbl/square.png);
      background-position: center center;
      /*background-size: 145% 140%;*/
      background-size: 150%;
      background-repeat: no-repeat;
    }


    .table-sqBooth-original{
      background: url(resources/img/tbl/Square-Booth.png);

      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-sqBooth{
      background: url(resources/img/tbl/Square-Booth.png);

      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-rectangle-original{
      background: url(resources/img/tbl/Rectangle.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-rectangle{
      height: 55px;
      background: url(resources/img/tbl/Rectangle.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-rBooth-original{
      background: url(resources/img/tbl/Rectangle-booth.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-rBooth{
      height: 55px;
      background: url(resources/img/tbl/Rectangle-booth.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-cTable-original{
      background: url(resources/img/tbl/Circle.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-cTable{
      background: url(resources/img/tbl/Circle.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .table-cBooth-original{
      background: url(resources/img/tbl/Circle-Booth.png);
      background-position: center center;
      background-size: 70%;
      background-repeat: no-repeat;
    }
    .table-cBooth{
      background: url(resources/img/tbl/Circle-Booth.png);
      background-position: center center;
      background-size: 100%;
      background-repeat: no-repeat;
    }


    .ui-widget-content{
      border:none;
    }
    .chair,.chair-original{
      background: #444;

      background-size:cover;
      width: 30px;
      height:30px;
      background-position: -6px -1px;
      border-radius: 200px;
    }
    .chair-original{
      padding: 24px;
      position: relative;
      margin: auto;
      top: 10px;
    }
    #ItemInventory{
      width: 100%;
      overflow-x: scroll;
    }
    .innerScroll{
      width: 300%;
    }
    .bg-area{
      width: 80%;
      height: 800px;

      background: url(http://dinetimehost.com/img/features/Features-Floor1.jpg);
      background-size: 100% 100%;
      background-position: 0 0;
    }

    .show {
      z-index:1000;
      position: absolute;
      background-color:#C0C0C0;
      border: 1px solid blue;
      padding: 2px;
      display: block;
      margin: 0;
      list-style-type: none;
      list-style: none;
    }

    .hide {
      display: none;
    }

    #rmenu ul{
      padding:10px;
      margin: 0px;
    }
  /*.seat{*/
    /*background: red;*/
    /*width: 20px;*/
    /*height: 20px;*/
  /*}*/
  .top{
    position:absolute;
    left: 20px;
    top:0;
  }
  .left{
    position: absolute;
    top: 20px;
    left: 0px;
  }
  .right{
    position: absolute;
    top: 20px;
    left: 40px;
  }
  .bottom{
    position: absolute;
    top: 40px;
    left: 20px;
  }
  .tNum{
    display: inline-block;
    background-color: white;
    border-radius: 1000px;
    width: 30px;
    height: 30px;
    font-size: 20px;
    position: absolute;
    text-align: center;
    top: 15px;
    left: 25px;
  }
    .show li{ list-style: none; }
    .show a { border: 0 !important; text-decoration: none; }
    .show a:hover { text-decoration: underline !important; }
    #success, #Failure{
      width: 100%;
      height: 100%;

      padding: 25% 30%;
      font-size: 30px;
      color: white;
      background:rgba(0,0,0,0.9);
      position: fixed;
      top: 0;
      left: 0;
      display: none;
      text-align: center;
    }
    button div{
      margin-bottom: 0px !important;
      margin-left: 0px !important;
      margin-top: 5px !important;
    }
    h3{
      text-align: center;
      border-bottom: thin solid #DADADA;
      padding-bottom: 10px;
    }
    #rotateTable{
      width: 20%;
      display: block;
      text-align: center;
      padding: 15px;
      background-color: #bbb;
      color: white;
      font-weight: bold;
      position: fixed;
      bottom: 50px;
      right: 0;
    }
    #submitLayout{
      width: 20%;
      display: block;
      text-align: center;
      padding: 15px;
      background-color: #AEAEF7;
      color: white;
      font-weight: bold;
      position: fixed;
      bottom: 0;
      right: 0;
    }
    #chairButton{
      top:25px;
    }
  </style>
  <script>
    var i = 0;
    var j = 0;
    var tCount = 1;
    $(function() {
      $( ".table" ).draggable({ snap: true });

      $(".tCreator").click(function(){
        if($(this).attr('data-item') == "chair-original"){
          $(".bg-area").append('<div data-eleType="1" id="seat-'+j+'" class="chair draggable ui-widget-content"></div>');
          $( ".chair" ).draggable({ snap: ".table", snapMode: "outer" });
          j++;
        }else{
          $(".bg-area").append('<div data-premium="0" data-eleType="'+$(this).attr('data-item')+'" id="ele-'+i+'"  class="'+$(this).attr('data-item')+' table draggable ui-widget-content"><p class="top seat"></p><p class="left seat"></p><p class="right seat"></p><p class="bottom seat"></p><p class="tNum">'+tCount+'</p></div>');
          $( "."+$(this).attr('data-item') ).draggable({ snap: ".table", snapMode: "outer" });
          tCount++;
          i++;

          $('.table').click(function(){
            $('#chosen').text($(this).attr('id'));
            if($(this).attr('data-premium') == 0){
              $("#premium").prop('checked', false);
            }else{
              $("#premium").prop('checked', true);
            }
          });
          var value = 0;
          $('#rotateTable').click(function(){
            value +=45;
            $("#"+$("#chosen").text()).rotate({ animateTo:value})
          });
        }
      });

    });

    $(document).ready(function() {
      $('footer').hide();
      var tableLayout = '<?=$tableLayout?>';
      var chairLayout = '<?=$chairLayout?>';

      if(tableLayout){
        var array = JSON.parse(tableLayout);
        array.forEach(function(object) {
          $(".bg-area").append('<div data-eleType="'+object.eleType+'" id="ele-'+i+'" class="'+object.eleType+' table draggable ui-widget-content"><p class="top seat"></p><p class="left seat"></p><p class="right seat"></p><p class="bottom seat"></p><p class="tNum">'+tCount+'</p></div>');

          $( ".table" ).draggable({ snap: ".table", snapMode: "outer",
            stop: function (event, ui) {
              /* Get the possible snap targets: */
              var snapped = $(this).data('ui-draggable').snapElements;

              /* Pull out only the snap targets that are "snapping": */
              var snappedTo = $.map(snapped, function (element) {
                return element.snapping ? element : null;
              });
              if(snappedTo[0]){
                $('#'+snappedTo[0]['item']['id']);
              }
            }
          });
          $('.table').click(function(){
            $('#chosen').text($(this).attr('id'));
            if($(this).attr('data-premium') == 0){
              $("#premium").prop('checked', false);
            }else{
              $("#premium").prop('checked', true);
            }
          });
          var value = 0;
          $('#rotateTable').click(function(){
            value +=45;
            $("#"+$("#chosen").text()).rotate({ animateTo:value})
          });
          $('#ele-'+i).attr('data-premium',object.premium);
          if(object.premium == 1){
            $('#ele-'+i).find(".tNum").css({
              'background':'red',
              'color':'white'
            });
          }
          $('#ele-'+i).css({
            'position':'absolute',
            'top':object.yPos+'px',
            'left':object.xPos+'px',
            '-webkit-transform' : object.rotate,
            '-moz-transform' : object.rotate,
            '-ms-transform' : object.rotate,
            '-o-transform' : object.rotate,
            'transform' : object.rotate,
            'zoom' : 1
          });
          tCount++;
          i++;
        });
      }

      if(chairLayout){
        var chairArray = JSON.parse(chairLayout);

        chairArray.forEach(function(object) {
          $(".bg-area").append('<div data-eleType="1" id="seat-'+j+'" class="chair draggable ui-widget-content"></div>');
          $( ".chair" ).draggable({ snap: ".table", snapMode: "outer" });
          $('#seat-'+j).css({
            'position':'absolute',
            'top':object.yPos+'px',
            'left':object.xPos+'px'
          });
          j++;
        });
      }


      $('#submitLayout').click(function(){
        var tables = [];
        var chairs = [];

        $('.table').each(function(){
          var offset = 0;
          if(
              $(this).css('transform') == "matrix(0.707107, 0.707107, -0.707107, 0.707107, 0, 0)" ||
              $(this).css('transform') == 'matrix(-0.707107, 0.707107, -0.707107, -0.707107, 0, 0)' ||
              $(this).css('transform') == "matrix(0.707107, -0.707107, 0.707107, 0.707107, 0, 0)" ||
              $(this).css('transform') == 'matrix(-0.707107, -0.707107, 0.707107, -0.707107, 0, 0)'
          ){
            offset = 10;
          }
          tables.push({
            xPos: $(this).offset().left + offset,
            yPos: $(this).offset().top + offset,
            rotate: $(this).css('transform'),
            premium: $(this).attr('data-premium'),
            eleType: $(this).attr('data-eleType')
          });
        });
        $('.chair').each(function(){
        chairs.push({
            xPos: $(this).offset().left,
            yPos: $(this).offset().top
          });
        });
        $.ajax({
          type: "POST",
          url: "/tableBuilder/editTableLayout?revId=<?=$_GET['revId']?>",
          data: {
            tableLayout: JSON.stringify(tables),
            chairLayout: JSON.stringify(chairs)
          },

          success: function(data, textStatus, jqXHR)
          {
            $('#success').show();
            $('#success').fadeOut(3000);
            window.location.replace("../snapshot?id=<?=$_GET['id'];?>");
          }
        });
        console.log(tables);
      });
//      $('nav').remove();
//      $('.jumbotron').removeAttr('class');
//      $('#footer').remove()
      $('.rFurn').click(function(){
        $('#'+$('.rFurn').attr('data-remove')).remove();
      });
      $('#premium').click(function(){
        if($('#'+$('#chosen').text()).attr(
                "data-premium"
            ) == 1){
          $('#'+$('#chosen').text()).attr(
              "data-premium","0"
          ).find('.tNum').css({
            'background':'white',
            'color':'black'
          });
        }else{
          $('#'+$('#chosen').text()).attr(
              "data-premium","1"
          ).find('.tNum').css({
            'background':'red',
            'color':'white'
          });
        }

      });


      if ($(".bg-area").addEventListener) {
        $(".bg-area").addEventListener('contextmenu', function(e) {
          alert("You've tried to open context menu"); //here you draw your own menu
          e.preventDefault();
        }, false);
      } else {

        //document.getElementById("test").attachEvent('oncontextmenu', function() {
        //$(".test").bind('contextmenu', function() {
        $('body').on('contextmenu', '.draggable', function() {
          $('.rFurn').attr('data-remove', $(this).attr('id'));

          //alert("contextmenu"+event);
          document.getElementById("rmenu").className = "show";
          document.getElementById("rmenu").style.top =  mouseY(event) + 'px';
          document.getElementById("rmenu").style.left = mouseX(event) + 'px';

          window.event.returnValue = false;


        });
      }

    });

    // this is from another SO post...
    $(document).bind("click", function(event) {
      document.getElementById("rmenu").className = "hide";
    });



    function mouseX(evt) {
      if (evt.pageX) {
        return evt.pageX;
      } else if (evt.clientX) {
        return evt.clientX + (document.documentElement.scrollLeft ?
                document.documentElement.scrollLeft :
                document.body.scrollLeft);
      } else {
        return null;
      }
    }

    function mouseY(evt) {
      if (evt.pageY) {
        return evt.pageY;
      } else if (evt.clientY) {
        return evt.clientY + (document.documentElement.scrollTop ?
                document.documentElement.scrollTop :
                document.body.scrollTop);
      } else {
        return null;
      }
    }
  </script>
</head>
<body>

<div id="ItemInventory">
  <h3>Furniture Creation (Click to create.)</h3>
  <div class="innerScroll">
    <!--  Tables-->

    <button class="btn btn-default tCreator table-original"  data-item="table-square" type="submit"><div>Square Table</div> </button>
    <button class="btn btn-default tCreator table-sqBooth-original"  data-item="table-sqBooth" type="submit"><div>Square Booth</div> </button>
    <button class="btn btn-default tCreator table-rectangle-original"  data-item="table-rectangle" type="submit"><div>Rectangle Table</div> </button>
    <button class="btn btn-default tCreator table-rBooth-original"  data-item="table-rBooth" type="submit"><div>Rectangle Booth</div> </button>
    <button class="btn btn-default tCreator table-cTable-original"  data-item="table-cTable" type="submit"><div>Circle Table</div> </button>
    <button class="btn btn-default tCreator table-cBooth-original"  data-item="table-cBooth" type="submit"><div>Circle Booth</div> </button>
    <button class="btn btn-default tCreator" style="background: none;" data-item="chair-original" type="submit"><p class="chair-original"></p><div id="chairButton">Chair</div></button>

    <!--  END Tables-->

  </div>





</div>

<br style="clear:both">

<div  class="bg-area">

</div>

<div id="success">Successfully saved table layout!</div>
<div id="Failure">Failed saving table layout</div>

<!-- initially hidden right-click menu -->
<div class="hide" id="rmenu">
  <ul>
    <li>
      <div class="rFurn">Remove Furniture</div>
    </li>
  </ul>
</div>


<div class="actionBar">
  <div class="actionLabel">
    Table Details
  </div>
  <div id="chosen"></div>
  <div>
    Table name: <input type="text" id="premiumCharge" placeholder="Bob's Table"/>
  </div>
  <div>
    Available for reservation: <input type="checkbox" id="reservation" />
  </div>
  <div>
    Premium Table: <input type="checkbox" id="premium" />
  </div>
  <div>
    Premium Charge: <input type="text" id="premiumCharge" placeholder="$50.00"/>
  </div>




  <div id="rotateTable">Rotate Table</div>
  <div id="submitLayout">Submit Layout</div>
</div>

</body>
</html>