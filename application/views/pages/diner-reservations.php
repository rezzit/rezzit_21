<div class="container">
    <?=$restaurant_sidebar?>
    <div class="col-md-6 col-md-pull-3 gridArea">
        <h2>Reports</h2>
        <div class="row">
            <div class="col-md-4">
                <h4>Financial Reports</h4>
                <div class="row">
                    <div class="col-md-12">
                        Revenue by Period
                    </div>
                    <div class="col-md-12">
                        Revenue by Category
                    </div>
                    <div class="col-md-12">
                        Revenue Comparison
                    </div>
                    <div class="col-md-12">
                        Revenue by Premium
                    </div>
                    <div class="col-md-12">
                        Projected Earnings
                    </div>
                    <div class="col-md-12">
                        Revenue by Vendor
                    </div>
                    <div class="col-md-12">
                        Pre-Ordered Revenue
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Reservation Reports</h4>
                <div class="row">
                    <div class="col-md-12">
                        Reservation Summary
                    </div>
                    <div class="col-md-12">
                        Projections
                    </div>
                    <div class="col-md-12">
                        Reservation Leadtime
                    </div>
                    <div class="col-md-12">
                        Table Summary
                    </div>
                    <div class="col-md-12">
                        Pre-Ordered Items
                    </div>
                    <div class="col-md-12">
                        Reservation by Time
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Diners Reports</h4>
                <div class="row">
                    <div class="col-md-12">
                        Reviews
                    </div>
                    <div class="col-md-12">
                        Custumer List
                    </div>
                    <div class="col-md-12">
                        Guest Totals
                    </div>
                    <div class="col-md-12">
                        Likes/Dislikes
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
