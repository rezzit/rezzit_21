<style>
    #main{
        background: white;
    }
    h4{
        margin-top: -5px;
    }
    .gridArea{
        border: thin solid #ccc;
        background: #dcdcdc;
        text-align: center;
        margin-top: 20px;
        font-size: 12px;
        padding: 20px;
    }
    .navBit{
        border-top: thin solid #ccc;
        padding: 0 20px;
        padding-top: 20px;
        margin: auto -20px;
        margin-bottom: 20px;
    }
    .gridArea img{
        border-radius: 100px;
        width: 200px;
        height: 200px;
        margin: auto;
        display: block;
    }
    .styled-select select {
        background: transparent;
        font-size: 12px;
        line-height: 1;
        border: 0;
        border-radius: 0;
        -webkit-appearance: none;
        color:black;
    }
    .styled-select {
        padding: 0;
        padding-right: 32px;
        overflow: hidden;
        background: #fff url("http://www.scottgood.com/jsg/blog.nsf/images/arrowdown.gif") no-repeat 98% 50%;
        border: 1px solid #ccc;
        /* background-position: center right; */
        margin: 3px;
        width: 100%;
    }
    input,select{
        width: 100% !important;
        display: block !important;
        padding: 10px;
        margin: 3px;
    }
</style>
<div class="col-md-3">
    <div class="gridArea">
        <h3>Welcome</h3>
        <img width="100%" src="https://pixabay.com/static/uploads/photo/2013/07/13/12/07/avatar-159236_640.png">
        <form method="post">
            <input type="file" accept="image/*" name="logo"></input>
        </form>
        <h4>Member Since: 6/30/2015</h4>
        <ul class="nav nav-sidebar">
            <li><a href="/diner-reservations">Reservations</a></li>
            <li><a href="/favorites">Favorites</a></li>
            <li><a href="/myaccount">Profile</a></li>
        </ul>
    </div>
</div>

