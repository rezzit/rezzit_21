<div class="container">

  <!-- Headings & Paragraph Copy -->
  <div class="row">
    <div class="col-md-6">
      <h3>Your Customers. Your Revenue.</h3>
      <p>Rezzit<sup>21</sup> is the modern, all-in-one customer management platform to run your business.</p>
      <button>More Info</button>
    </div>
    <div class="col-md-6">
      <img height="200" width="200" src="https://s3.amazonaws.com/rezzit21website/stock_photos/man-coffee-cup-pen.jpg">
    </div>
  </div>
  <div class="row">
    <div class="col-md-4">
      <h3>Take Control of Your Entire Experience</h3>
      <p>Rezzit21’s state of the art reservation process gives you a fully interactive way to easily plan your entire dining experience from any device, at any time. It’s more than just a reservation</p>
      <button>More Info</button>
    </div>
    <div class="col-md-4">
      <h3>Define Your Dining Experience</h3>
      <p>Rezzit21 allows you to pre-order anything from appetizers, decanted wine or special occasion flowers, to premium seating, and even extra time at your favorite table. Direct how and when the restaurant should present it at the table, and pay for those pre-ordered amenities ahead of time. Best of all? There are no booking or hidden fees. Rezzit21 is the dining app you’ve been waiting for.</p>
    </div>
    <div class="col-md-4">
      <h3>Take Control of Your Entire Experience</h3>
      <p>Rezzit21’s state of the art reservation process gives you a fully interactive way to easily plan your entire dining experience from any device, at any time. It’s more than just a reservation</p>
      <button>More Info</button>
    </div>
  </div>
 </div> 
